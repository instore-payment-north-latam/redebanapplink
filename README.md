# Instore Applinking

A brief description of what this project does and who it's for

## What's inside

InStore app executes its AppLinking feature
(Intent on Android or RCTLinking on iOS)
with the available actions—payment and payment-reversal—that are sent to the payment app integrated with inStore.

## Init

`npm install` - Install all dependencies

`Android Studio` - To emulate devices

`adb devices` - to show the connected devices in case you don't use emulator

This project is a webview content, all frontEnd screens were developed in the following repository. Deploy on aws s3

WebView repository:
https://gitlab.com/vtex-dev/applink-webview/-/tree/integrations

To do Deploy:

```
"deploy": "aws s3 sync out/ s3://dev-vtex-instore-applink-webview --acl public-read",
"deploydev": "aws s3 sync out/ s3://dev-vtex-instore-applink-webview --acl public-read"
```

## Build

Use Android studio or the follow commans

```
npm install
npm run start
npm run android
cd android && ./gradlew clean && ./gradlew cleanBuildCache && cd ..
```

## Generate Apk

```
cd android && ./gradlew assembleRelease && cd ..
```

Find apk in app/buil/outputs/apk

## Aditional info

For more information visit this link`s

- https://github.com/vtex/instore-applinking-payment-test/blob/master/docs/acquirers.en.md
- https://help.vtex.com/es/tracks/instore-usando-o-app--4BYzQIwyOHvnmnCYQgLzdr/2rPSJ8519UCCZo5uEBkqxh
- https://docs.google.com/document/d/1dTMcpq5p45wtqh7h3rEICeaprooRz3S8AoDePpfMg9w/edit
