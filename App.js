import React, {Component} from 'react';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import ProcessingScreen from './screens/ProcessingScreen';
import HomeScreen from './screens/HomeScreen';
import SettingsScreen from './screens/SettingsScreen';
import RedebanScreen from './screens/RedebanScreen';
import VtexScreen from './screens/VtexScreen';
import CustomScreen from './screens/CustomScreen';
import NewPayment from './screens/NewPayment';
import ScanScreen from './screens/ScanScreen';
import PusherScreen from './screens//PusherScreen';
import NewSettingsScreen from './screens/NewSettingsScreen';
import ConfigDevice from './screens/ConfigDevice';
import PayTransaction from './screens/PayTransaction';
import Index from './screens/Index';
import NewScannerScreen from './screens/NewScannerScreen';
import PrintScreen from './screens/PrintScreen';
import ManualSettings from './screens/ManualSettings';
import QRSettings from './screens/QRSettings';
const MainNavigator = createStackNavigator({
  Index: {screen: Index},
  NewPayment: {screen: NewPayment},
  PrintScreen: {screen: PrintScreen},
  NewSettingsScreen: {screen: NewSettingsScreen},
  HomeScreen: {screen: HomeScreen},
  ProcessingScreen: {screen: ProcessingScreen},
  SettingsScreen: {screen: SettingsScreen},
  RedebanScreen: {screen: RedebanScreen},
  VtexScreen: {screen: VtexScreen},
  ScanScreen: {screen: ScanScreen},
  PusherScreen: {screen: PusherScreen},
  ConfigDevice: {screen: ConfigDevice},
  PayTransaction: {screen: PayTransaction},
  NewScannerScreen: {screen: NewScannerScreen},
  ManualSettings: {screen: ManualSettings},
  QRSettings: {screen: QRSettings},
  CustomScreen: {screen: CustomScreen},
});

const App = createAppContainer(MainNavigator);

export default App;
