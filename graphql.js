import gql from 'graphql-tag';

export const CREATE_TRANSACTION = gql`
  mutation createTransaction(
    $orderId: String!
    $paymentId: String!
    $transactionId: String!
    $sellerName: String!
    $payerEmail: String!
    $payerIdentification: String
    $amount: Float!
    $clientId: Float!
  ) {
    createTransaction(
      input: {
        orderId: $orderId
        paymentId: $paymentId
        transactionId: $transactionId
        sellerName: $sellerName
        payerEmail: $payerEmail
        payerIdentification: $payerIdentification
        amount: $amount
        clientId: $clientId
      }
    ) {
      amount
      status
      paymentId
    }
  }
`;

export const GET_PAYMENT_BY_RECEIPT = gql`
  query getPaymentByReceipt($paymentId: String!) {
    getPaymentByReceipt(paymentId: $paymentId) {
      status
      response {
        response
        approveCode
        cardNumbers
        franchise
      }
      responseAttempts
    }
  }
`;
