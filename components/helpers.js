import React, {Component} from 'react';

const helpers = {
  // {/*
  //     NGROK: https://d1l3cxyw0qa7am.cloudfront.net
  //     PRO: http://vtex-instore-applink-webview.s3-website-us-east-1.amazonaws.com/
  //     DEV: http://dev-vtex-instore-applink-webview.s3-website-us-east-1.amazonaws.com/
  // */}
  urlWebview:
    'http://dev-vtex-instore-applink-webview.s3-website-us-east-1.amazonaws.com/',
  urlTransaction: 'https://transactions.vtexnorthlatam.com/api/transactions',
  urlSurge: 'https://appwebview-redeban.surge.sh/', // This url come to ApplinkWebView, view README

  validateValueUrl: function (value) {
    var validate = false;
    var re = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    if (re.test(value.text)) {
      validate = true;
    } else {
      validate = false;
    }
    return validate;
  },

  validateValueDatafono: function (value) {
    validate = false;
    if (value.text.length < 5) {
      var validate = true;
    } else {
      validate = false;
    }
    return validate;
  },
};

export default helpers;

// let urlTemp = 'redeban://payment?acquirerProtocol=custom&action=payment&installmentType=2&installments=1&paymentType=debit&amount=400000&installmentsInterestRate=0&scheme=instore&autoConfirm=True&acquirerFee=0&paymentId=ABB0664DDE08450495D2BFD122C873A7&paymentSystem=44&paymentSystemName=Venda%20Direta%20Debito&paymentGroupName=debitDirectSalePaymentGroup&sellerName=newbusinessunits&transactionId=0CD33907999B412194D0B10611F86BB9&value=100000&orderGroupId=1122960709071&reference=1122960709071&payerIdentification=&payerEmail=1617725462661-anonymous%40vtex.com&urlCallback=instore://payment'
