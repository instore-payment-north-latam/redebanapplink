import React, { Component } from 'react'
import { Text, View, Button } from 'react-native'
import Dialog, { 
   SlideAnimation, 
   DialogContent, 
   DialogTitle, 
   DialogFooter, 
   DialogButton 
 } from 'react-native-popup-dialog';
 import Icon from "react-native-vector-icons/Ionicons";
 import {dialogStyle, globalStyle} from '../assets/styles/styles';

export default class DialogConfirm extends Component {

   constructor (props) {
      super(props);
      this.state = {
         visibleDialog: false,
      };

      try {
         var params = props.navigation.state.params.myTest;
         console.log('params: ', params);
      } catch (error) {
         console.log('nel');
      }
   }

   render() {
      return (
         <View>
          <Button
            title="Show Dialog"
            onPress={() => {
               this.setState({ visibleDialog: true });
            }}
          />
          <Dialog
            visible={this.state.visibleDialog}
            dialogAnimation={new SlideAnimation({
                slideFrom: 'bottom',
            })}
            footer={
              <DialogFooter>
                <DialogButton
                  text="Anular"
                  onPress={() => { this.setState({ visibleDialog: false }) }}
                />
                <DialogButton
                  text="Continuar"
                  onPress={() => { this.props.buildUrlReturn() }}
                />
              </DialogFooter>
            }        
            onTouchOutside={() => {
               this.setState({ visibleDialog: false });
            }}>
            <DialogContent style={dialogStyle.contentDialog}>
              <View style={dialogStyle.headerDialog}>
                <Text style={dialogStyle.title}>Confirmación</Text>
              </View>
              <View>
                  <View style={dialogStyle.icon}>
                     <Icon 
                        name="md-checkmark"
                        color="#F71963"
                        size={90}
                     />
                  </View>
                <Text style={ globalStyle.textCenter }>La compra ha sido confirmada</Text>
              </View>
            </DialogContent>
          </Dialog>
         </View>
      )
   }
}
