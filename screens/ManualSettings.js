import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {approvedStyle} from '../assets/styles/styles';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-async-storage/async-storage';

let infoDeviceModel;
export default class ManualSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idDevice: '',
    };
  }

  handleIdDevice = text => {
    this.setState({idDevice: text});
  };

  initSetting = idDevice => {
    let dataUniqueId = DeviceInfo.getUniqueId();
    let dataModel = DeviceInfo.getModel();
    let dataSystemName = DeviceInfo.getSystemName();
    let dataBrand = DeviceInfo.getBrand();
    let dataSystemVersion = DeviceInfo.getSystemVersion();
    let dataDeviceType = DeviceInfo.getDeviceType();

    let dataDevice = {
      deviceID: dataUniqueId,
      deviceType: dataDeviceType,
      deviceModel: dataModel,
      dataBrand: dataBrand,
      os: dataSystemName,
      osVersion: dataSystemVersion,
    };

    let useUrl = 'https://pubsub.vtexnorthlatam.com:9850/pub?id=' + idDevice;
    axios.post(useUrl, dataDevice).then(response => {
      const ws = new WebSocket(
        'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + idDevice,
      );
      ws.onmessage = event => {
        let data = JSON.parse(event.data);
        if (data._id) {
          // Save DATA in Storage
          if (data.deviceModel == 'A920' || data.deviceModel == 'A910') {
            infoDeviceModel = 'deeplinking-redeban';
          } else {
            infoDeviceModel = 'webservice-redeban-pay';
          }
          this.saveDataStore('transactionDeviceId', data._id);
          this.saveDataStore('transactionPaymentType', infoDeviceModel);
          this.saveDataStore(
            'registryModel',
            data.paymentMethods.redeban.model,
          );
          this.saveDataStore('dataIva', data.iva);

          this.setState({allData: data});
          this.setState({showInfo: true});
        }
        this.props.onMessage && this.props.onMessage(event);
      };
      this.setState({ws});
    });
  };

  saveDataStore = async (keySave, valueSave) => {
    ç;
    try {
      await AsyncStorage.setItem(keySave.toString(), valueSave.toString());
    } catch (e) {
      console.log('Error save data');
    }
  };

  renderHome = () => {
    const {navigate} = this.props.navigation;
    navigate('Index');
  };

  static navigationOptions = {header: null};
  render() {
    return (
      <View style={[approvedStyle.contentButtonsScan, styles.custom]}>
        {!this.state.showInfo ? (
          <View>
            <Text style={styles.paragraphSubTitle}>Configuración Manual</Text>
            <View style={styles.container2}>
              <TextInput
                style={styles.input}
                underlineColorAndroid="transparent"
                placeholder="ID dispositivo"
                autoCapitalize="none"
                onChangeText={this.handleIdDevice}
              />

              <TouchableOpacity
                style={styles.btnScann}
                onPress={() => this.initSetting(this.state.idDevice)}>
                <Text style={styles.textBtn}>Iniciar</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <View style={styles.paragraph}>
            <Text style={styles.paragraphTitle}>Dispositivo Registrado</Text>
            <Text style={styles.paragraphSubTitle}>Información General</Text>
            <Text style={styles.paragraph}>
              Base de impuestos: {this.state.allData.iva}
            </Text>
            <Text style={styles.paragraph}>
              Seller: {this.state.allData._sellerID}
            </Text>
            <Text style={styles.paragraph}>
              Modelo: {this.state.allData.paymentMethods.redeban.model}
            </Text>
            <Text style={styles.paragraph}>ID: {this.state.allData._id}</Text>
            <View style={approvedStyle.alignButtons}>
              <TouchableOpacity
                style={styles.btnContinue}
                onPress={this.renderHome}>
                <Text style={styles.txtContinue}>Continuar</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
  },
  input: {
    margin: 15,
    height: 50,
    borderWidth: 1,
    borderRadius: 5,
    width: 250,
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },

  custom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFF',
    textAlign: 'center',
  },

  container2: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  textBtn: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Fabriga',
    fontSize: 18,
    marginVertical: 8,
  },

  paragraphTitle: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 24,
    color: '#142032',
    fontWeight: 'bold',
    marginVertical: 30,
  },

  paragraph: {
    fontFamily: 'Fabriga',
    textAlign: 'left',
    fontSize: 16,
    marginVertical: 8,
  },
  txtContinue: {
    fontFamily: 'Fabriga',
    color: '#FFF',
    textAlign: 'center',
    fontSize: 25,
    fontWeight: 'bold',
  },
  paragraphSubTitle: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 20,
    marginVertical: 6,
  },

  btnScann: {
    backgroundColor: '#2953b2',
    marginTop: 40,
    width: 148,
    padding: 6,
    borderRadius: 4,
    marginHorizontal: 6,
  },
  btnContinue: {
    margin: 10,
    width: 200,
    backgroundColor: '#8bc34a',
    paddingVertical: 14,
    borderRadius: 5,
  },
});
