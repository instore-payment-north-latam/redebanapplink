import React, {Component} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  Linking,
  TouchableOpacity,
} from 'react-native';
import {buildFailedResponseUrl} from '../utils';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';

import * as UrlNode from 'url';
import DeviceInfo from 'react-native-device-info';
import RNExitApp from 'react-native-exit-app';

let togglePayRedeban = '',
  togglePayMercado,
  togglePayScanner,
  showOptionalPayment = '',
  makeTransactionLane = '',
  registryModel;
let valueModel = '',
  dataUrl,
  dataTerminal,
  dataMpKey,
  dataMpToken,
  dataCheckPrint,
  dataParamTitle,
  dataParamNit,
  dataParamAddress,
  dataParamNameStore,
  dataParamImage,
  countPay = 0,
  webUrl = false,
  payMode;
var renHome;
global.urlGlobal = 'url';
let lowBattery = false;

export default class NewSettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allValuesData: [],
      countPay: 0,
      withUrl: false,
      adroid5: false,
      showOptionalPayment: false,
      makeTransactionLane: false,
    };

    this.webView = null;
    this.onMessage = this.onMessage.bind(this);
    this.onMessagePay = this.onMessagePay.bind(this);
    this._handleOpenURL = this._handleOpenURL.bind(this);
  }

  componentDidMount() {
    console.log('first');
    this.setState({version: DeviceInfo.getVersion()});
    DeviceInfo.getBatteryLevel().then((batteryLevel) => {
      if (batteryLevel != 1) {
        batteryLevel = parseInt(batteryLevel.toFixed(2).split('.')[1]);
        if (batteryLevel != 1 && batteryLevel < 10) {
          lowBattery = true;
        }
      }
    });

    var getSystemName = DeviceInfo.getSystemName();
    var getSystemVersion = DeviceInfo.getSystemVersion();

    if (getSystemName == 'Android' && getSystemVersion.includes('5.1')) {
      this.setState({adroid5: true});
    }

    this.handleLoad();
    // this.validateUrl();
  }

  returnLowBattery() {
    Linking.openURL(
      buildFailedResponseUrl(
        '',
        'La batería del dispositivo es muy baja para hacer la transferencia',
      ),
    );
    RNExitApp.exitApp();
  }

  _handleOpenURL(event) {
    if (event && event.url) {
      var url = event.url;
      urlGlobal = url;
      var urlOpen = UrlNode.parse(url, true);
      this.setState({withUrl: true});
      this.renderFunction(urlOpen);
    }
  }

  validateAllData() {
    this.validateUrl();
  }

  validateUrl() {
    Linking.getInitialURL()
      .then((_handleInitialUrl) => {
        if (_handleInitialUrl) {
          url = _handleInitialUrl;
          urlGlobal = url;
          console.log('Url is: ' + _handleInitialUrl);
          const urlParams = UrlNode.parse(_handleInitialUrl, true);
          this.setState({withUrl: true});
          this.renderFunction(urlParams);
        } else {
          this.setState({withUrl: false});
          console.log('No there Url');
        }
      })
      .catch((err) => console.error('An error occurred', err));
    Linking.addEventListener('url', this._handleOpenURL);
  }

  renderFunction(urlParams) {
    // console.log('renderFunction');
    // console.log('countPay', countPay);
    // console.log('URL', this.state.withUrl);
    // console.log('webUrl', webUrl);
    // console.log('payMode', payMode);

    DeviceInfo.getDeviceName().then((deviceName) => {
      if (deviceName.indexOf('A920') > -1) {
        this.storeData('setPayMode', 'redeban');
        this.rendering(urlParams);
      } else if (registryModel) {
        const {navigate} = this.props.navigation;
        navigate('PayTransaction');
      } else {
        if (countPay == 1) {
          this.storeData('setPayMode', payMode);
          this.rendering(urlParams);
          this.setState({showOptionalPayment: false});
        } else if (countPay >= 2) {
          this.setState({showOptionalPayment: true});
          showOptionalPayment = 'redirect';
        }
      }
    });
  }

  rendering(urlParams) {
    renHome = urlParams.query.home;

    if (lowBattery) {
      this.returnLowBattery();
    } else {
      if (!renHome) {
        const {navigate} = this.props.navigation;
        navigate('ProcessingScreen');
      }
    }
  }

  handleLoad = async (loadDataPay) => {
    try {
      const dataNameStore = await AsyncStorage.getItem('dataNameStore');
      const dataValueType = await AsyncStorage.getItem('dataValueType');
      const dataValueCountry = await AsyncStorage.getItem('dataValueCountry');
      const dataIva = await AsyncStorage.getItem('dataIva');
      const dataPayMode = await AsyncStorage.getItem('dataPayMode');
      registryModel = await AsyncStorage.getItem('registryModel');

      /**
       * Medios de Pago
       */
      togglePayRedeban = await AsyncStorage.getItem('togglePayRedeban');
      togglePayMercado = await AsyncStorage.getItem('togglePayMercado');
      togglePayScanner = await AsyncStorage.getItem('togglePayScanner');

      if (togglePayRedeban == 'on') {
        countPay = countPay + 1;
        valueModel = await AsyncStorage.getItem('valueModel');
        dataUrl = await AsyncStorage.getItem('dataUrl');
        dataTerminal = await AsyncStorage.getItem('dataTerminal');
        payMode = 'redeban';
      }
      if (togglePayMercado == 'on') {
        countPay = countPay + 1;
        dataMpKey = await AsyncStorage.getItem('dataMpKey');
        dataMpToken = await AsyncStorage.getItem('dataMpToken');
        payMode = 'mercadopago';
      }
      if (togglePayScanner == 'on') {
        countPay = countPay + 1;
        dataCheckPrint = await AsyncStorage.getItem('dataCheckPrint');
        dataParamTitle = await AsyncStorage.getItem('dataParamTitle');
        dataParamNit = await AsyncStorage.getItem('dataParamNit');
        dataParamAddress = await AsyncStorage.getItem('dataParamAddress');
        dataParamNameStore = await AsyncStorage.getItem('dataParamNameStore');
        dataParamImage = await AsyncStorage.getItem('dataParamImage');
        payMode = 'scanner';
      }

      // Validate if contains multiple pay methods
      this.setState({countPay: countPay});

      this.validateAllData();
      this.sendPostMessage(
        dataNameStore,
        dataValueType,
        dataValueCountry,
        dataIva,
        dataPayMode,
      );
    } catch (error) {
      console.log('Error in handleLoad');
    }
  };

  onMessage(event) {
    var data = event.nativeEvent.data;
    console.log('On Message', data);

    this.onMessagePay(data);

    if (data == 'loadData') {
      this.handleLoad();
    } else {
      var dataArray = data.split('&');
      for (let userObject of dataArray) {
        var dataStore = userObject.split('=');
        if (dataStore[1]) {
          this.storeData(dataStore[0], dataStore[1]);
        }
      }
    }
  }

  onMessagePay(data) {
    const {navigate} = this.props.navigation;
    if (data == 'a920') {
      navigate('NewPayment');
    } else if (data == 'lane') {
      this.storeData('setPayMode', 'lane');
      navigate('ProcessingScreen');
    } else if (data == 'scanner') {
      navigate('ScanScreen');
    }
  }

  storeData = async (keySave, valueSave) => {
    if (keySave == 'setPayMode') {
      console.log('setPayMode is set', valueSave);
    }
    console.log('storeData: ', keySave, valueSave);
    try {
      await AsyncStorage.setItem(keySave.toString(), valueSave.toString());
      console.log('Success save data');
    } catch (e) {
      console.log('Error save data');
    }
  };

  sendPostMessage(
    dataNameStore,
    dataValueType,
    dataValueCountry,
    dataIva,
    dataPayMode,
  ) {
    console.log(
      'Sending post message',
      dataNameStore,
      dataValueType,
      dataValueCountry,
      dataIva,
      dataPayMode,
    );

    var allData = {
      showOptionalPayment: showOptionalPayment,
      makeTransactionLane: makeTransactionLane,
      dataNameStore: dataNameStore,
      dataValueType: dataValueType,
      dataValueCountry: dataValueCountry,
      dataIva: dataIva,
      dataPayMode: dataPayMode,
      paySettings: {
        togglePayRedeban: togglePayRedeban,
        valueModel: valueModel,
        dataUrl: dataUrl,
        dataTerminal: dataTerminal,
        togglePayMercado: togglePayMercado,
        dataMpKey: dataMpKey,
        dataMpToken: dataMpToken,
        togglePayScanner: togglePayScanner,
        dataCheckPrint: dataCheckPrint,
        dataParamTitle: dataParamTitle,
        dataParamNit: dataParamNit,
        dataParamAddress: dataParamAddress,
        dataParamNameStore: dataParamNameStore,
        dataParamImage: dataParamImage,
      },
    };

    console.log(allData);
    this.webView.postMessage(JSON.stringify(allData));
  }

  renderLoadingView() {
    return <ActivityIndicator size="large" color="#F71963" />;
  }

  static navigationOptions = {header: null};

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex: 1}}>
        {/* <TouchableHighlight style={{padding: 10, backgroundColor: '#142032'}} onPress={() => this.sendData()}>
               <Text style={{color: 'white', textAlign:'center'}}>Cargar Datos</Text>
            </TouchableHighlight>   */}

        {/*
               https://d1l3cxyw0qa7am.cloudfront.net
               http://vtex-instore-applink-webview.s3-website-us-east-1.amazonaws.com/
               http://dev-vtex-instore-applink-webview.s3-website-us-east-1.amazonaws.com/
            */}

        <WebView
          source={{
            uri:
              'http://dev-vtex-instore-applink-webview.s3-website-us-east-1.amazonaws.com/',
          }}
          ref={(webView) => (this.webView = webView)}
          onMessage={this.onMessage}
          renderLoading={this.renderLoadingView}
          startInLoadingState={true}
          onLoad={() => this.handleLoad()}
          androidLayerType="hardware"
        />

        {this.state.adroid5 && (
          <TouchableOpacity
            style={{padding: 10, backgroundColor: '#142032'}}
            onPress={() => navigate('ConfigDevice')}>
            <Text style={{color: 'white', textAlign: 'center'}}>
              Configuración
            </Text>
          </TouchableOpacity>
        )}

        <Text style={{textAlign: 'right'}}>version: {this.state.version} </Text>
      </View>
    );
  }
}
