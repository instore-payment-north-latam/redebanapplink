import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import BackgroundTimer from 'react-native-background-timer';

let intervalId
class CustomScreen extends React.Component {
   
   componentDidMount() {
      this.setData()
   }

   setData() {

      intervalId = BackgroundTimer.setInterval(() => {
         console.log('tic', intervalId);
      }, 5000);

      // BackgroundTimer.runBackgroundTimer(() => { 
      //    console.log('BBB')
      // }, 5000);
      //rest of code will be performing for iOS on background too
   }

   cancel() {
      console.log('Cancel', )
      BackgroundTimer.clearInterval(intervalId);
   }
  
   render() {
   return (
      <View>
         <Text>Hello world</Text>
         <TouchableOpacity onPress={() => this.cancel() }>
            <Text>cancelar</Text>
         </TouchableOpacity>
      </View>
   )
   }
}

export default CustomScreen