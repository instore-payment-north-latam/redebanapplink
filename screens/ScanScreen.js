import React, {Component} from 'react';
import {
  Text,
  View,
  Linking,
  Button,
  NativeModules,
  NativeEventEmitter,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import {buildResponseUrl, buildFailedResponseUrl} from '../utils';
import {clone} from 'ramda';
import RNExitApp from 'react-native-exit-app';
import {RNCamera} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import * as UrlNode from 'url';
import Spinner from 'react-native-spinkit';
import {
  newPaymentStyle,
  approvedStyle,
  globalStyle,
} from '../assets/styles/styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import {WebView} from 'react-native-webview';
import helpers from '../components/helpers';

var numberConfirm;
var url;
var reference;
var renHome;
var isPrint;
var printValue;
var printSellerName;
var printEmail;
var printIdentification;
let dataParamTitle,
  dataParamNit,
  dataParamAddress,
  dataParamNameStore,
  dataParamModel;
let codeIdScann;

export default class ScanScreen extends Component {
  constructor(props) {
    super(props);

    this.camera = null;
    this.barcodeCodes = [];

    this.state = {
      urlIncoming: false,
      value: false,
      approvedScann: false,
      camera: {
        type: RNCamera.Constants.Type.back,
        flashMode: RNCamera.Constants.FlashMode.on,
      },
    };

    this.reRender = this.props.navigation.addListener('willFocus', () => {
      this.handleLoad();
    });

    this._handleInitialUrl = this._handleInitialUrl.bind(this);
    this.onBarCodeRead = this.onBarCodeRead.bind(this);
  }

  _handleInitialUrl(urlIncoming) {
    if (urlIncoming) {
      console.log('URL is: ' + url);
      this.setState({urlIncoming: urlIncoming});
    }
  }

  send = (data) => this.state.ws.send(data);
  componentDidMount() {
    this.reconnect = !!this.props.reconnect;
    // this._handleWebSocketSetup()

    console.log(numberConfirm);
    this.setValues(urlGlobal);

    const eventEmitter = new NativeEventEmitter(NativeModules.ToastExample);
    eventEmitter.addListener('printResponse', (event) => {
      if (event == 'printSuccess') {
        this._returnUrl();
      }
    });

    this.handleLoad();
  }

  // componentWillUnmount () {
  //       this.reconnect = false
  //       this.state.ws.close()
  // }

  _handleWebSocketSetup = () => {
    const ws = new WebSocket(
      'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + codeIdScann,
    );
    ws.onopen = (e) => {
      console.log('OPEN', e);
      this.props.onOpen && this.props.onOpen();
    };
    ws.onmessage = (event) => {
      console.log('Message from websocket');
      console.log(event);
      this.props.onMessage && this.props.onMessage(event);
    };
    ws.onerror = (error) => {
      console.log('error');
      this.props.onError && this.props.onError(error);
    };
    ws.onclose = () =>
      this.reconnect
        ? this._handleWebSocketSetup()
        : this.props.onClose && this.props.onClose();
    this.setState({ws});
  };

  handleLoad = async () => {
    try {
      const value = await AsyncStorage.getItem('dataCheckPrint');
      dataParamTitle = await AsyncStorage.getItem('dataParamTitle');
      dataParamNit = await AsyncStorage.getItem('dataParamNit');
      dataParamAddress = await AsyncStorage.getItem('dataParamAddress');
      dataParamNameStore = await AsyncStorage.getItem('dataParamNameStore');
      dataParamModel = await AsyncStorage.getItem('valueModel');

      if (value !== null) {
        isPrint = value;
        console.log('isPrint:', isPrint);
      } else {
        console.log('Error get data - AsyncStorage');
      }
    } catch (e) {
      // error reading value
    }
  };

  setValues(url) {
    console.log('set all values');
    const urlParams = UrlNode.parse(url, true);

    reference = urlParams.query.reference;
    printSellerName = urlParams.query.sellerName;
    printValue = urlParams.query.value;
    printIdentification = urlParams.query.payerIdentification;
    printEmail = urlParams.query.payerEmail;

    renHome = urlParams.query.home;
    if (renHome) {
      console.log(renHome);
      this.renderHome();
    }
    console.log('reference:', reference);
  }

  renderHome() {
    const {navigate} = this.props.navigation;
    navigate('NewSettingsScreen');
  }

  _returnUrl() {
    console.log('Build Url');
    const tempUrl = clone(urlGlobal);

    Linking.openURL(
      buildResponseUrl(tempUrl, '000', 'Qr code Free', 'Qr code Free'),
    );
    RNExitApp.exitApp();
  }

  onBarCodeRead(scanResult) {
    if (scanResult.data != null) {
      if (!this.barcodeCodes.includes(scanResult.data)) {
        var scanValue = scanResult.data;
        console.log('scanValue: ', scanValue);
        this.setState({approvedScann: true});
        if (scanValue == 'https://instore.vtex.com') {
          if (isPrint == 'checked') {
            console.log('printing...');
            this.onPressPrint();
          } else {
            console.log('NO printing...');
            this._returnUrl();
          }
        } else {
          codeIdScann = scanValue;
          let dataUniqueId = DeviceInfo.getUniqueId();
          let dataModel = DeviceInfo.getModel();
          let dataSystemName = DeviceInfo.getSystemName();
          let dataBrand = DeviceInfo.getBrand();
          let dataSystemVersion = DeviceInfo.getSystemVersion();
          let dataDeviceType = DeviceInfo.getDeviceType();
          let infoDeviceModel;

          let dataDevice = {
            deviceID: dataUniqueId,
            deviceType: dataDeviceType,
            deviceModel: dataModel,
            dataBrand: dataBrand,
            os: dataSystemName,
            osVersion: dataSystemVersion,
          };

          let useUrl =
            'https://pubsub.vtexnorthlatam.com:9850/pub?id=' + codeIdScann;
          console.log(dataUniqueId, dataModel, dataSystemName, useUrl);

          axios.post(useUrl, dataDevice).then((response) => {
            const ws = new WebSocket(
              'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + codeIdScann,
            );
            ws.onmessage = (event) => {
              let data = JSON.parse(event.data);
              if (data._id) {
                // Save DATA in Storage
                if (data.deviceModel == 'A920') {
                  infoDeviceModel = 'deeplinking-redeban';
                } else {
                  infoDeviceModel = 'webservice-redeban-pay';
                }

                this.saveDataStore('transactionDeviceId', data._id);
                this.saveDataStore('transactionPaymentType', infoDeviceModel);
                this.saveDataStore(
                  'registryModel',
                  data.paymentMethods.redeban.model,
                );

                this.setState({allData: data});
                this.setState({showInfo: true});
              }
              this.props.onMessage && this.props.onMessage(event);
            };
            this.setState({ws});
          });
        }
      }
    }
    return;
  }

  onPressPrint() {
    console.log('Imprimiendo...');
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    var hours = new Date().getHours();
    var min = new Date().getMinutes();
    var allDate = date + '/' + month + '/' + year;
    var allTime = hours + ':' + min;

    if (printEmail.indexOf('anonymous') > -1) {
      printEmail = 'anonymous';
    }

    console.log(date.toString());
    NativeModules.ActivityStarter.imprimir(
      dataParamTitle,
      dataParamNit,
      dataParamAddress,
      dataParamNameStore,
      'Recibo de Venta',
      reference,
      dataParamModel,
      allDate.toString(),
      allTime.toString(),
      printEmail,
      printIdentification,
      printValue,
      'ScannerQR',
      'www.vtex.com/co-es/',
      printSellerName,
    );
  }

  switchType = () => {
    let newType;
    const {back, front} = RNCamera.Constants.Type;

    if (this.state.camera.type === back) {
      newType = front;
    } else if (this.state.camera.type === front) {
      newType = back;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        type: newType,
      },
    });
  };

  changeToScann = () => {
    this.setState({approvedScann: false});
  };

  saveDataStore = async (keySave, valueSave) => {
    console.log('Save Storage Info: ', keySave, valueSave);
    try {
      await AsyncStorage.setItem(keySave.toString(), valueSave.toString());
      console.log('Success save data');
    } catch (e) {
      console.log('Error save data');
    }
  };

  renderLoadingView() {
    return <ActivityIndicator size="large" color="#134cd8" />;
  }

  render() {
    let typeButton;
    if (this.state.camera.type === RNCamera.Constants.Type.back) {
      typeButton = (
        <Button
          onPress={this.switchType}
          title="Cambiar modo (Frontal)"
          color="#134cd8"
        />
      );
    } else {
      typeButton = (
        <Button
          onPress={this.switchType}
          title="Cambiar modo (Trasera)"
          color="#134cd8"
        />
      );

      return (
        <View style={styles.container}>
          {!this.state.approvedScann && (
            <RNCamera
              ref={(ref) => {
                this.camera = ref;
              }}
              defaultTouchToFocus
              flashMode={this.state.camera.flashMode}
              mirrorImage={false}
              onBarCodeRead={this.onBarCodeRead.bind(this)}
              onFocusChanged={() => {}}
              onZoomChanged={() => {}}
              style={styles.cameraView}
              type={this.state.camera.type}>
              <BarcodeMask
                edgeColor={'#FFFFFF'}
                animatedLineColor={'#FFFFFF'}
              />
              {/* <View style={[styles.overlay, styles.bottomOverlay]}>
                     <Text style={styles.custom}>Escanee código QR</Text>
                  </View> */}
              {typeButton}
            </RNCamera>
          )}

          {this.state.approvedScann && (
            <View style={[approvedStyle.contentButtonsScan, styles.custom]}>
              {/* <TouchableOpacity
                     style={[approvedStyle.buttons, approvedStyle.cancelButton]}
                     onPress = { this.onPressPrint } >
                     <Text style={globalStyle.centerWhite}>Imprimir</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                     style={approvedStyle.buttons}
                     onPress = { this._returnUrl } >
                     <Text style={globalStyle.centerWhite}>Continuar</Text>
                  </TouchableOpacity> */}

              {!this.state.showInfo && (
                <View style={[approvedStyle.contentButtonsScan, styles.custom]}>
                  <Spinner color="#134cd8" size={80} type="Circle" />

                  <TouchableOpacity
                    style={[approvedStyle.btnScann]}
                    onPress={this.changeToScann}>
                    <Text
                      style={[approvedStyle.btnScannText, styles.paragraph]}>
                      Scanner
                    </Text>
                  </TouchableOpacity>
                </View>
              )}

              {this.state.showInfo && (
                <View style={styles.paragraph}>
                  <Text style={styles.paragraphTitle}>
                    Dispositivo Registrado
                  </Text>
                  <Text style={styles.paragraph}>Información General</Text>
                  <Text style={styles.paragraph}>
                    Id: {this.state.allData._id}
                  </Text>
                  <Text style={styles.paragraph}>
                    Seller: {this.state.allData._sellerID}
                  </Text>
                  <Text style={styles.paragraph}>
                    Creación: {this.state.allData.createdAt}
                  </Text>
                  <Text style={styles.paragraph}>
                    Modelo: {this.state.allData.paymentMethods.redeban.model}
                  </Text>

                  <WebView
                    source={{
                      uri: 'https://f117bde4b40f.ngrok.io/payTransaction/',
                    }}
                    ref={(webView) => (this.webView = webView)}
                    renderLoading={this.renderLoadingView}
                    startInLoadingState={true}
                    androidLayerType="hardware"
                    style={{flex: 1}}
                  />
                </View>
              )}
            </View>
          )}
        </View>
      );
    }

    const styles = StyleSheet.create({
      custom: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        color: '#FFF',
        textAlign: 'center',
      },

      container: {
        flex: 1,
      },

      overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center',
      },

      bottomOverlay: {
        bottom: 0,
        backgroundColor: '#F71963',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },

      cameraView: {
        flex: 1,
        justifyContent: 'flex-start',
      },

      paragraph: {
        fontFamily: 'Fabriga',
        fontSize: 18,
        textAlign: 'center',
      },
      paragraphTitle: {
        fontFamily: 'Fabriga',
        textAlign: 'center',
        fontSize: 24,
        color: '#F71963',
        fontWeight: 'bold',
        marginVertical: 30,
      },
    });
  }
}
