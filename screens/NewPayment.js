import React, {Component} from 'react';
import {
  Text,
  View,
  Linking,
  TouchableOpacity,
  StyleSheet,
  NativeModules,
  NativeEventEmitter,
  ActivityIndicator,
} from 'react-native';
import Spinner from 'react-native-spinkit';
import {
  newPaymentStyle,
  approvedStyle,
  globalStyle,
} from '../assets/styles/styles';
import * as UrlNode from 'url';
import {buildResponseUrl, buildFailedResponseUrl} from '../utils';
import {clone} from 'ramda';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info';
import axios from 'axios';
import helpers from '../components/helpers';
import BackgroundTimer from 'react-native-background-timer';

let jsonTransactionStr,
  numberConfirm,
  cardNumber,
  cardName,
  dateRequest,
  approveCode,
  receiptCode,
  ivaValue,
  accountType,
  installmentsPayment,
  intervalTransaction;

export default class NewPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: false,
      msmTimeOut: false,
      approvedPay: false,
      mmsError: false,
      txtError: '',
      monto: '',
      timer: 60,
      transactionID: '',
      intervalContinue: '',
      noTransaction: false,
      msmNoTransaction: '',
    };

    this._returnFailedUrl = this._returnFailedUrl.bind(this);
    this._onInvalid = this._onInvalid.bind(this);
    this.openComercios = this.openComercios.bind(this);
  }

  componentDidUpdate() {
    if (this.state.timer === 0) {
      //Clean contdown interval
      clearInterval(this.interval);
    }
  }

  /**
   * Wait redeban Response
   */
  componentDidMount() {
    this.setValues(urlGlobal);
    const eventEmitter = new NativeEventEmitter(NativeModules.ToastExample);
    eventEmitter.addListener('comerciosResponse', event => {
      const responseInfo = JSON.parse(event);
      numberConfirm = responseInfo.Numero_confirmacion;
      if (this.state.transactionID) {
        this.updateTransaction(responseInfo);
      }
    });
  }

  /**
   * Set urlGlobal empty to prevent repeat transaction
   * Go Home screen
   */
  renderHome = async () => {
    urlGlobal = '';
    const {navigate} = this.props.navigation;
    navigate('Index');
    console.log('-> Navigate index');
  };

  /**
   * If the payment is approved init Button countDown
   */
  successPayment(responseInfo) {
    cardNumber = responseInfo.Ultimos_digitos_tarjeta;
    cardName = responseInfo.Franquicia;
    dateRequest = responseInfo.Fecha_hora;
    accountType = responseInfo.Tipo_cuenta;
    installmentsPayment = responseInfo.Numero_cuotas;
    approveCode = responseInfo.Numero_confirmacion;
    receiptCode = responseInfo.Numero_recibo;
    this.setState({approvedPay: true});

    if (this.state.approvedPay) {
      console.log('-> Inital Count');
      this.state.intervalContinue = BackgroundTimer.setInterval(() => {
        this._returnUrl();
      }, 60000);
    }
    this.interval = setInterval(
      () => this.setState(prevState => ({timer: prevState.timer - 1})),
      1000,
    );
  }

  /**
   * Define values to Iva, base, monto...
   * Iva value get from deviceStorage, if it doesn't  the defaul value is 19
   */
  setValues = async url => {
    try {
      const value = await AsyncStorage.getItem('dataIva');
      if (value !== null) {
        ivaValue = value;
      } else {
        ivaValue = 19;
      }
    } catch (e) {
      console.error('Error get Iva');
    }
    const urlParams = UrlNode.parse(url, true);
    this.setState({amount: urlParams.query.value});

    let iva_monto_base = 1 + ivaValue / 100;
    let iva_monto = ivaValue / 100;
    let monto = parseFloat(
      this.state.amount.substring(0, this.state.amount.length - 2),
    );
    let monto_base_iva = Math.round(monto / iva_monto_base);
    let iva = Math.floor(monto_base_iva * iva_monto);
    let inc = 0;
    let monto_base_inc = 0;
    let base_devolucion = Math.floor(monto_base_iva);

    this.setState({monto: monto.toString()});
    this.setState({iva: iva.toString()});
    this.setState({inc: inc.toString()});
    this.setState({monto_base_iva: monto_base_iva.toString()});
    this.setState({monto_base_inc: monto_base_inc.toString()});
    this.setState({base_devolucion: base_devolucion.toString()});

    this.createTransaction(url);
  };

  /**
   * Open Redeban Comercios app, ActivityStarter is developer with java
   */
  openComercios() {
    let jsonTransaction = {
      TipoTransaccion: 1,
      properties: {},
    };

    jsonTransaction.properties = {
      Monto: this.state.monto,
      Iva: this.state.iva,
      Inc: this.state.inc,
      Monto_base_iva: this.state.monto_base_iva,
      Monto_base_inc: this.state.monto_base_inc,
      Base_devolucion: this.state.base_devolucion,
    };

    jsonTransactionStr = JSON.stringify(jsonTransaction);
    DeviceInfo.getDeviceName().then(deviceName => {
      if (deviceName.indexOf('A920') > -1 || deviceName.indexOf('A910') > -1) {
        NativeModules.ActivityStarter.navigateComercios(jsonTransactionStr);
      } else {
        this.setState({
          txtError:
            'No se pudo finalizar el pago - Error al abrir App comercios',
        });
        this._returnFailedUrl();
      }
    });
  }

  /**
   * Create inital transaction. this transaction will be modified after with the transactionID
   * Default valuo to status is PENDING
   * payerIdentification is default
   */
  createTransaction = async url => {
    let taxRate = (parseFloat(ivaValue) / 100).toFixed(2);
    let transactionDeviceId = await AsyncStorage.getItem('transactionDeviceId');
    let transactionPaymentType = await AsyncStorage.getItem(
      'transactionPaymentType',
    );

    if (transactionDeviceId && transactionPaymentType) {
      const urlParams = UrlNode.parse(url, true);
      let payerIdentification = '2243';
      if (urlParams.query.payerIdentification) {
        payerIdentification = urlParams.query.payerIdentification;
      }
      let infoTransaction = {
        orderId: urlParams.query.orderGroupId,
        paymentId: urlParams.query.paymentId,
        transactionId: urlParams.query.transactionId,
        deviceId: transactionDeviceId,
        paymentType: transactionPaymentType,
        payerEmail: urlParams.query.payerEmail,
        payerIdentification: payerIdentification,
        amount: urlParams.query.value / 100,
        taxRate: taxRate,
        debugMode: true,
      };

      const config = {
        method: 'post',
        url: helpers.urlTransaction,
        headers: {
          'Content-Type': 'application/json',
        },
        data: infoTransaction,
      };

      axios(config)
        .then(response => {
          let data = response.data.data;
          if (data._id) {
            this.setState({transactionID: data._id});
            this.openComercios();
          }
        })
        .catch(error => {
          console.error('Failed createTransaction: ', error.response);
          this.setState({noTransaction: true});
          this.setState({
            msmNoTransaction: error.response.data.error
              ? error.response.data.error + ' - ' + error.response.data.message
              : 'Error al crear la transacción',
          });
        });
    }
  };

  /**
   * UpdateTransaction, we use it to define transaction status, this can be:
   * Anulled, if app comercios response error
   * Approved, if everything is ok
   * Rejected, if app comercios or endpoints response error
   */
  async updateTransaction(data) {
    let statusTransaction, amount;
    amount = this.state.amount ? this.state.amount : 555;
    if (data.Tipo_operacion == 2 && !data.Mensaje_error) {
      statusTransaction = 'annulled';
    } else if (data.Numero_confirmacion && !data.Mensaje_error) {
      statusTransaction = 'approved';
    } else {
      statusTransaction = 'rejected';
    }

    if (data.timeout == 'timeout') {
      this.setState({msmTimeOut: true});
      BackgroundTimer.clearInterval(intervalTransaction);
      data.Mensaje_error = 'TimeOut';
    }

    const patchTransaction = {
      status: statusTransaction,
      response: {
        idPaymentRequest: this.state.transactionID,
        approveCode: data.Codigo_respuesta,
        date: data.Fecha_hora,
        franchise: data.Franquicia,
        messageResponse: data.Mensaje_error,
        confirmNumber: data.Numero_confirmacion,
        numberInstallments: data.Numero_cuotas,
        receiptNumber: data.Numero_recibo,
        accountType: data.Tipo_cuenta,
        operationType: data.Tipo_operacion,
        cardNumbers: data.Ultimos_digitos_tarjeta,
        amount: amount,
        taxes: ivaValue,
      },
    };

    const configUpdata = {
      method: 'patch',
      url: helpers.urlTransaction,
      headers: {
        'Content-Type': 'application/json',
      },
      data: patchTransaction,
    };

    try {
      await axios(configUpdata).then(data => {
        console.log('Success updateTransaction: ', data);
      });
    } catch (error) {
      console.error('Failed updateTransaction: ', error);
    }
    this.validateShowInfo(data);
  }

  /**
   * ValidateShowInfo: Depending on App comercios or this App response display information
   */
  validateShowInfo(responseInfo) {
    if (responseInfo.Tipo_operacion == 2 && !responseInfo.Mensaje_error) {
      this.setState({txtError: 'Pago Anulado'});
      this._returnFailedUrl();
    } else if (numberConfirm && !responseInfo.Mensaje_error) {
      this.setState({mmsError: false});
      this.successPayment(responseInfo);
    } else {
      if (responseInfo.Tipo_operacion != 2 && !this.state.msmTimeOut) {
        this.setState({mmsError: true});
      }
      this.setState({txtError: responseInfo.Mensaje_error});
    }
  }
  /**
   * If transaction is payment will be annullad open again App comercios with new data
   */
  _onInvalid = async () => {
    let jsonInvalid = {
      TipoTransaccion: 2,
      properties: {},
    };
    jsonInvalid.properties = {
      Numero_recibo: receiptCode,
    };
    const jsonInvalidStr = JSON.stringify(jsonInvalid);
    this.clearBackgroundTimer(false);
    NativeModules.ActivityStarter.navigateComercios(jsonInvalidStr);
  };

  /**
   * If everything is ok, return to Instore with all transaction Data
   */
  _returnUrl = async () => {
    if (this.state.approvedPay) {
      const tempUrl = clone(urlGlobal);
      await Linking.openURL(
        buildResponseUrl(
          tempUrl,
          cardNumber,
          cardName,
          approveCode,
          dateRequest,
          receiptCode,
          accountType,
          installmentsPayment,
        ),
      );
      this.clearBackgroundTimer();
      this.renderHome();
    }
  };

  /**
   * If it fails, return to Instore with error
   */
  _returnFailedUrl = async () => {
    const textError = this.state.txtError;
    this.clearBackgroundTimer();
    Linking.openURL(buildFailedResponseUrl('10', textError.toString())).then(
      () => {
        this.renderHome();
      },
    );
  };

  /**
   * Clean Background timer to prevent repeat transaction
   */
  clearBackgroundTimer = (clearID = true) => {
    if (clearID) {
      this.setState({transactionID: ''});
    }

    this.setState({
      value: false,
      msmTimeOut: false,
      approvedPay: false,
      mmsError: false,
      txtError: '',
      monto: '',
      intervalContinue: '',
      noTransaction: false,
      msmNoTransaction: '',
    });

    BackgroundTimer.stopBackgroundTimer();
    BackgroundTimer.stopBackgroundTimer(this.state.intervalContinue);
    BackgroundTimer.clearInterval(this.state.intervalContinue);
    BackgroundTimer.clearInterval(intervalTransaction);
    console.log('-> Clear all data');
  };

  renderLoadingView() {
    return <ActivityIndicator size="large" color="#F71963" />;
  }

  static navigationOptions = {header: null};
  render() {
    return (
      <View style={newPaymentStyle.container}>
        {!this.state.mmsError && !this.state.approvedPay && (
          <View style={styles.container}>
            <Spinner color="#F71963" size={100} type="ThreeBounce" />
            <Text style={[globalStyle.textCenter, styles.paragraph]}>
              Procesando pago...
            </Text>

            {this.state.noTransaction && (
              <View>
                <Text style={styles.timer}>La transacción no fue creada</Text>
                <Text style={styles.timer}>
                  Mensaje: {this.state.msmNoTransaction}
                </Text>
                <View style={approvedStyle.contentButtons}>
                  <TouchableOpacity
                    style={[approvedStyle.buttons, approvedStyle.cancelButton]}
                    onPress={() => {
                      this.setState({noTransaction: false});
                      this.setValues(urlGlobal);
                    }}>
                    <Text style={globalStyle.centerWhite}>Reintentar</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        )}

        {this.state.mmsError && (
          <View>
            <Text style={[globalStyle.textCenter, styles.paragraphTitle]}>
              Error en el pago
            </Text>
            <Text style={globalStyle.textCenter}>
              Mensaje: {this.state.txtError}
            </Text>
            <View style={approvedStyle.contentButtons}>
              <TouchableOpacity
                style={[approvedStyle.buttons, approvedStyle.cancelButton]}
                onPress={this._returnFailedUrl}>
                <Text style={globalStyle.centerWhite}>Cancelar</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {this.state.approvedPay && (
          <View>
            <Text style={[globalStyle.textCenter, styles.paragraphTitle]}>
              PAGO EXITOSO
            </Text>
            <Text style={[globalStyle.textCenter, styles.paragraph]}>
              Número de Recibo: {receiptCode}
            </Text>
            <Text style={[globalStyle.textCenter, styles.paragraph]}>
              Número de Confirmación: {numberConfirm}
            </Text>

            <View style={approvedStyle.alignButtons}>
              <Text style={styles.timer}>
                El proceso continuará en: {this.state.timer} segundos{' '}
              </Text>
              <TouchableOpacity
                style={styles.btnContinue}
                onPress={this._returnUrl}>
                <Text style={[styles.txtContinue]}>Continuar</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnCancel}
                onPress={this._onInvalid}>
                <Text style={styles.txtCancel}>Anular</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paragraphTitle: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    color: '#142032',
    fontSize: 24,
    fontWeight: 'bold',
    marginVertical: 18,
  },
  fontFabriga: {
    fontFamily: 'Fabriga',
  },
  paragraph: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 20,
    marginVertical: 4,
  },
  timer: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 14,
    marginVertical: 6,
  },
  txtContinue: {
    fontFamily: 'Fabriga',
    color: '#FFF',
    textAlign: 'center',
    fontSize: 25,
    fontWeight: 'bold',
  },
  btnContinue: {
    margin: 10,
    width: 200,
    backgroundColor: '#8bc34a',
    paddingVertical: 14,
    borderRadius: 5,
    marginTop: 30,
  },
  txtCancel: {
    fontFamily: 'Fabriga',
    color: '#FFF',
    textAlign: 'center',
    fontSize: 16,
  },
  btnCancel: {
    width: 100,
    backgroundColor: '#F71963',
    padding: 10,
    borderRadius: 5,
    marginTop: 30,
  },
});
