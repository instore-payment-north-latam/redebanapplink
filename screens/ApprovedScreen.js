import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import {approvedStyle, globalStyle} from '../assets/styles/styles';
import Icon from "react-native-vector-icons/Ionicons";

export default class ApprovedScreen extends Component {

   constructor(props) {
      super(props);
      this.state = {};
   }

   render() {
      return (
         <View>
            <View style={approvedStyle.headerDialog}>
               <Text style={approvedStyle.title}>Orden Confirmada</Text>
            </View>
            <View>
               <View style={approvedStyle.icon}>
               <Icon 
                  name="md-checkmark"
                  color="#F71963"
                  size={100}
               />
               </View>

               <View style={approvedStyle.contentButtons}>
                  <TouchableOpacity
                     style={[approvedStyle.buttons, approvedStyle.cancelButton]}
                     onPress = {() => this.props.onInvalid()} >
                     <Text style={globalStyle.centerWhite}>Anular</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                     style={approvedStyle.buttons}
                     onPress = {() => this.props.buildUrlReturn()} >
                     <Text style={globalStyle.centerWhite}>Continuar</Text>
                  </TouchableOpacity>
               </View>
            </View>
         </View>
      )
   }
}
