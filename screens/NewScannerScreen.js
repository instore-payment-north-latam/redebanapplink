import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {approvedStyle} from '../assets/styles/styles';

export default class NewScannerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  readQR = () => {
    const {navigate} = this.props.navigation;
    navigate('QRSettings');
  };

  manualSettings = () => {
    const {navigate} = this.props.navigation;
    navigate('ManualSettings');
  };

  static navigationOptions = {header: null};
  render() {
    return (
      <View style={[approvedStyle.contentButtonsScan, styles.custom]}>
        <Text style={styles.paragraphSubTitle}>Tipo de configuración</Text>
        <View style={styles.container}>
          <TouchableOpacity style={styles.btnScann} onPress={this.readQR}>
            <Text style={styles.textBtn}>Código QR</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btnScann}
            onPress={this.manualSettings}>
            <Text style={styles.textBtn}>Manual</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = {
  custom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFF',
    textAlign: 'center',
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  textBtn: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Fabriga',
    fontSize: 18,
    marginVertical: 8,
  },

  paragraphTitle: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 24,
    color: '#142032',
    fontWeight: 'bold',
    marginVertical: 30,
  },

  paragraphSubTitle: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 20,
    marginVertical: 6,
  },

  btnScann: {
    backgroundColor: '#2953b2',
    marginTop: 40,
    width: 148,
    padding: 6,
    borderRadius: 4,
    marginHorizontal: 6,
  },
};
