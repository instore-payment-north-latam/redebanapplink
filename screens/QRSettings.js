import React, {Component} from 'react';
import {Text, View, TouchableOpacity, ActivityIndicator} from 'react-native';
import {RNCamera} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import Spinner from 'react-native-spinkit';
import {approvedStyle} from '../assets/styles/styles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import {Icon} from 'react-native-elements';

let codeIdScann, infoDeviceModel;
export default class NewScannerScreen extends Component {
  constructor(props) {
    super(props);
    this.camera = null;
    this.barcodeCodes = [];
    this.state = {
      approvedScann: false,
      camera: {
        type: RNCamera.Constants.Type.back,
        flashMode: RNCamera.Constants.FlashMode.on,
      },
      onLoadWebview: true,
    };
    this.webView = null;
    this.onBarCodeRead = this.onBarCodeRead.bind(this);
    this.onMessage = this.onMessage.bind(this);
  }
  send = data => this.state.ws.send(data);
  onBarCodeRead(scanResult) {
    if (scanResult.data != null) {
      if (!this.barcodeCodes.includes(scanResult.data)) {
        let scanValue = scanResult.data;
        this.setState({approvedScann: true});
        if (scanValue) {
          codeIdScann = scanValue;
          let dataUniqueId = DeviceInfo.getUniqueId();
          let dataModel = DeviceInfo.getModel();
          let dataSystemName = DeviceInfo.getSystemName();
          let dataBrand = DeviceInfo.getBrand();
          let dataSystemVersion = DeviceInfo.getSystemVersion();
          let dataDeviceType = DeviceInfo.getDeviceType();

          let dataDevice = {
            deviceID: dataUniqueId,
            deviceType: dataDeviceType,
            deviceModel: dataModel,
            dataBrand: dataBrand,
            os: dataSystemName,
            osVersion: dataSystemVersion,
          };
          let useUrl =
            'https://pubsub.vtexnorthlatam.com:9850/pub?id=' + codeIdScann;
          axios.post(useUrl, dataDevice).then(response => {
            const ws = new WebSocket(
              'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + codeIdScann,
            );
            ws.onmessage = event => {
              let data = JSON.parse(event.data);
              if (data._id) {
                // Save DATA in Storage
                if (data.deviceModel == 'A920' || data.deviceModel == 'A910') {
                  infoDeviceModel = 'deeplinking-redeban';
                } else {
                  infoDeviceModel = 'webservice-redeban-pay';
                }
                this.saveDataStore('transactionDeviceId', data._id);
                this.saveDataStore('transactionPaymentType', infoDeviceModel);
                this.saveDataStore(
                  'registryModel',
                  data.paymentMethods.redeban.model,
                );
                this.saveDataStore('dataIva', data.iva);

                this.setState({allData: data});
                this.setState({showInfo: true});
              }
              this.props.onMessage && this.props.onMessage(event);
            };
            this.setState({ws});
          });
        }
      }
    }
    return;
  }
  sendDataWebView(data, infoDeviceModel) {
    let infoTransaction = {
      transactionDeviceId: data._id,
      transactionPaymentType: infoDeviceModel,
      registryModel: data.paymentMethods.redeban.model,
      sellerID: data._sellerID,
      createdAt: data.createdAt,
      openScreen: 'sacannerScreen',
    };
    this.webView.postMessage(JSON.stringify(infoTransaction));
  }
  switchType = () => {
    let newType;
    const {back, front} = RNCamera.Constants.Type;

    if (this.state.camera.type === back) {
      newType = front;
    } else if (this.state.camera.type === front) {
      newType = back;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        type: newType,
      },
    });
  };
  saveDataStore = async (keySave, valueSave) => {
    try {
      await AsyncStorage.setItem(keySave.toString(), valueSave.toString());
    } catch (e) {
      console.error('Error save data');
    }
  };
  renderLoadingView() {
    return (
      <View style={{flex: 1}}>
        <ActivityIndicator size="large" color="#134cd8" />
      </View>
    );
  }
  webviewLoad() {
    this.setState({onLoadWebview: false});
  }

  webviewLoadEnd() {
    if (this.state.showInfo) {
      this.sendDataWebView(this.state.allData, infoDeviceModel);
    }
  }
  onMessage(event) {
    let data = event.nativeEvent.data;
    if (data == 'retryScanner') {
      this.setState({approvedScann: false});
    }
  }
  renderHome = () => {
    const {navigate} = this.props.navigation;
    navigate('Index');
  };

  changeToScann = () => {
    this.setState({approvedScann: false});
  };

  static navigationOptions = {header: null};
  render() {
    let typeButton;
    if (this.state.camera.type === RNCamera.Constants.Type.back) {
      typeButton = (
        <View style={styles.customBtn}>
          <Icon
            name="cached"
            size={46}
            onPress={this.switchType}
            color="#FFF"
          />
        </View>
      );
    } else {
      typeButton = (
        <View style={styles.customBtn}>
          <Icon
            name="cached"
            size={46}
            onPress={this.switchType}
            color="#FFF"
          />
        </View>
      );
    }

    return (
      <View style={{flex: 1}}>
        {!this.state.approvedScann && (
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            defaultTouchToFocus
            flashMode={this.state.camera.flashMode}
            mirrorImage={false}
            onBarCodeRead={this.onBarCodeRead.bind(this)}
            onFocusChanged={() => {}}
            onZoomChanged={() => {}}
            style={styles.cameraView}
            type={this.state.camera.type}>
            <BarcodeMask edgeColor={'#d9d9d9'} animatedLineColor={'#d9d9d9'} />
            {typeButton}
          </RNCamera>
        )}
        {this.state.approvedScann && (
          <View style={[approvedStyle.contentButtonsScan, styles.custom]}>
            {!this.state.showInfo ? (
              <View style={[approvedStyle.contentButtonsScan, styles.custom]}>
                <Spinner color="#134cd8" size={80} type="Circle" />
                <TouchableOpacity
                  style={styles.btnScann}
                  onPress={this.changeToScann}>
                  <Text style={styles.textBtn}>Volver a leer código QR</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.paragraph}>
                <Text style={styles.paragraphTitle}>
                  Dispositivo Registrado
                </Text>
                <Text style={styles.paragraphSubTitle}>
                  Información General
                </Text>
                <Text style={styles.paragraph}>
                  Base de impuestos: {this.state.allData.iva}
                </Text>
                <Text style={styles.paragraph}>
                  Seller: {this.state.allData._sellerID}
                </Text>
                <Text style={styles.paragraph}>
                  Modelo: {this.state.allData.paymentMethods.redeban.model}
                </Text>
                <Text style={styles.paragraph}>
                  ID: {this.state.allData._id}
                </Text>
                <View style={approvedStyle.alignButtons}>
                  <TouchableOpacity
                    style={styles.btnContinue}
                    onPress={this.renderHome}>
                    <Text style={styles.txtContinue}>Continuar</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}
const styles = {
  custom: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFF',
    textAlign: 'center',
  },

  container: {
    flex: 1,
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: '#F71963',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraView: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  textBtn: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Fabriga',
    fontSize: 20,
    marginVertical: 8,
  },
  paragraph: {
    fontFamily: 'Fabriga',
    textAlign: 'left',
    fontSize: 16,
    marginVertical: 8,
  },
  paragraphSubTitle: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 20,
    marginVertical: 6,
  },
  paragraphTitle: {
    fontFamily: 'Fabriga',
    textAlign: 'center',
    fontSize: 24,
    color: '#142032',
    fontWeight: 'bold',
    marginVertical: 30,
  },
  customBtn: {
    position: 'absolute',
    marginVertical: 30,
    bottom: 0,
    left: 'auto',
    right: 40,
    backgroundColor: '#000',
    color: '#FFFFFF',
    borderRadius: 50,
    padding: 10,
  },
  txtContinue: {
    fontFamily: 'Fabriga',
    color: '#FFF',
    textAlign: 'center',
    fontSize: 25,
    fontWeight: 'bold',
  },
  btnContinue: {
    margin: 10,
    width: 200,
    backgroundColor: '#8bc34a',
    paddingVertical: 14,
    borderRadius: 5,
  },
  btnScann: {
    backgroundColor: '#134cd8',
    marginTop: 40,
    width: 250,
    padding: 15,
    borderRadius: 5,
  },
};
