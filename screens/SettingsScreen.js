import React, {Component} from 'react';
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Picker,
} from 'react-native';
import Collapsible from 'react-native-collapsible';
import {styles} from '../assets/styles/styles';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Divider from 'react-native-divider';

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      showElement: false,
      collapsed: true,
      collapsedMp: true,
      datastoreName: null,
      dataCountry: null,
      dataType: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.sendForm = this.sendForm.bind(this);
  }

  componentDidMount() {
    this.handleLoad();
    this.props.navigation.setParams({sendForm: this.sendForm});
  }

  handleLoad = async () => {
    console.log('In Settings Screen');
    try {
      const storeName = await AsyncStorage.getItem('storeName');
      const dataCountry = await AsyncStorage.getItem('dataCountry');
      const dataType = await AsyncStorage.getItem('dataType');
      if (storeName !== null && dataCountry !== null && dataType !== null) {
        this.setState({datastoreName: storeName});
        this.setState({dataCountry: dataCountry});
        this.setState({dataType: dataType});

        if (dataCountry == 'colombia') {
          this.setState({showElement: true});
        }
      }
    } catch (error) {
      console.log('No found eleement in storage');
    }
  };

  handleChange(target) {
    var stateName = target._dispatchInstances.memoizedProps.name;
    var stateValue = target.nativeEvent.text;
    this.setState({
      [stateName]: stateValue,
    });
  }

  toggleExpanded = () => {
    this.setState({collapsed: !this.state.collapsed});
  };

  toggleExpandedMp = () => {
    this.setState({collapsedMp: !this.state.collapsedMp});
  };

  sendForm = async () => {
    var datastoreName = this.state.datastoreName;
    var dataCountry = this.state.dataCountry;
    var dataType = this.state.dataType;
    if (datastoreName && dataCountry && dataType) {
      this.setState({onValidate: false});
      try {
        await AsyncStorage.setItem('storeName', this.state.datastoreName);
        await AsyncStorage.setItem('dataCountry', this.state.dataCountry);
        await AsyncStorage.setItem('dataType', this.state.dataType);
        Toast.showWithGravity(
          'Información guardada correctamente.',
          Toast.LONG,
          Toast.CENTER,
        );
      } catch (error) {
        alert('error');
      }
    } else {
      this.setState({onValidate: true});
    }
    // console.log(this.state.datastoreName +' '+ this.state.dataCountry+' '+ this.state.dataRedeUrl+' '+ this.state.dataRedeDatafono);
  };

  static navigationOptions = ({navigation}) => ({
    title: 'Configuración',
    headerRight: (
      <TouchableOpacity
        style={styles.btnSave}
        onPress={navigation.getParam('sendForm')}>
        <Text style={styles.textSave}>Guardar</Text>
      </TouchableOpacity>
    ),
  });

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <ScrollView>
          <Divider color="#F71963" orientation="center">
            GENERAL
          </Divider>
          <Text>Nombre de la tienda</Text>
          <TextInput
            style={styles.inputCustom}
            name="datastoreName"
            value={this.state.datastoreName}
            onChange={this.handleChange}
          />
          {this.state.onValidate && (
            <Text style={styles.msmRequired}>
              <Text style={styles.msmSpanRequired}>*</Text>Este campo es
              requerido
            </Text>
          )}

          <Text>Tipo</Text>
          <View style={styles.inputCustom}>
            <Picker
              style={styles.customPicker}
              selectedValue={this.state.dataType}
              onValueChange={value => {
                this.setState({dataType: value});
              }}>
              <Picker.Item label="Seleccione un tipo" />
              <Picker.Item label="Tótem" value="totem" />
              <Picker.Item label="Venta asitida" value="ventaasistida" />
            </Picker>
          </View>

          <Text>País</Text>
          <View style={styles.inputCustom}>
            <Picker
              style={styles.customPicker}
              selectedValue={this.state.dataCountry}
              onValueChange={value => {
                this.setState({dataCountry: value});
                if (value == 'colombia') {
                  this.setState({showElement: true});
                } else {
                  this.setState({showElement: false});
                }
              }}>
              <Picker.Item label="Seleccione un país" />
              <Picker.Item label="Colombia" value="colombia" />
              <Picker.Item label="Ecuador" value="ecuador" />
              <Picker.Item label="Perú" value="peru" />
              <Picker.Item label="Panamá" value="panama" />
            </Picker>
          </View>
          {this.state.onValidate && (
            <Text style={styles.msmRequired}>
              <Text style={styles.msmSpanRequired}>*</Text>Este campo es
              requerido
            </Text>
          )}

          {this.state.showElement && (
            <ScrollView style={styles.sectionCollapsed}>
              <View style={styles.contentCollapsed}>
                <TouchableOpacity onPress={() => navigate('RedebanScreen')}>
                  <Text>Redeban</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.separeCollapsed} />
            </ScrollView>
          )}

          <Divider color="#F71963" orientation="center">
            VTEX
          </Divider>
          <TouchableOpacity onPress={() => navigate('VtexScreen')}>
            <Text>Configuración de VTEX</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

export default SettingsScreen;
