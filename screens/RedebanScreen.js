import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  CheckBox,
  TouchableOpacity,
  Picker,
} from 'react-native';
import {styles} from '../assets/styles/styles';
import helpers from '../components/helpers';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class RedebanScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      showSave: true,
      validateUrl: false,
      dataRedeUrl: null,
      dataRedeIdDatafono: null,
      checkedRedeban: false,
      checkedPrint: false,
      dataRedeModel: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.saveData = this.saveData.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
  }

  componentDidMount() {
    this.handleLoad();
    this.props.navigation.setParams({saveData: this.saveData});
  }

  handleLoad = async () => {
    console.log('In Redeban Settings');
    try {
      const dataRedeUrl = await AsyncStorage.getItem('dataRedeUrl');
      const dataRedeIdDatafono = await AsyncStorage.getItem(
        'dataRedeIdDatafono',
      );
      const dataRedeModel = await AsyncStorage.getItem('dataRedeModel');
      const checkedRedeban = await AsyncStorage.getItem('checkedRedeban');
      const checkedPrint = await AsyncStorage.getItem('checkedPrint');
      var isTrueSet = checkedRedeban == 'true';
      var isTrueSetPrint = checkedPrint == 'true';
      if (dataRedeUrl) {
        this.setState({dataRedeUrl: dataRedeUrl});
        this.setState({dataRedeIdDatafono: dataRedeIdDatafono});
        this.setState({dataRedeModel: dataRedeModel});
        this.setState({checkedRedeban: isTrueSet});
        this.setState({checkedPrint: isTrueSetPrint});
        this.setState({validateUrl: true});
        this.props.navigation.setParams({Display: 'flex'});
      }

      if (dataRedeModel == 'scan') {
        this.setState({scannerPrint: true});
      }
    } catch (error) {
      console.log('No found eleement in storage');
    }
  };

  saveData = async () => {
    var dataRedeUrl = this.state.dataRedeUrl;
    var dataRedeIdDatafono = this.state.dataRedeIdDatafono;
    var dataRedeModel = this.state.dataRedeModel;
    var validateUrl = this.state.validateUrl;
    var checkedPrint = this.state.checkedPrint;

    console.log(':::Save Data:::');
    console.log(dataRedeUrl);
    console.log(dataRedeIdDatafono);
    console.log(dataRedeModel);
    console.log(validateUrl);
    console.log(checkedPrint);
    console.log('::::::::::::');

    if (dataRedeUrl && dataRedeIdDatafono && validateUrl) {
      try {
        await AsyncStorage.setItem('dataRedeUrl', dataRedeUrl);
        await AsyncStorage.setItem('dataRedeIdDatafono', dataRedeIdDatafono);
        await AsyncStorage.setItem('dataRedeModel', dataRedeModel);
        await AsyncStorage.setItem('checkedRedeban', 'true');
        await AsyncStorage.setItem('checkedPrint', checkedPrint.toString());
        Toast.showWithGravity(
          'Información Redeban guardada correctamente.',
          Toast.LONG,
          Toast.CENTER,
        );
      } catch (error) {
        alert('No se pudo guardar la información');
      }
      this.setState({onValidate: false});
      this.setState({validateShowUrl: false});
    } else {
      this.setState({onValidate: true});
      this.setState({validateShowUrl: true});
    }
  };

  handleChange(target) {
    var stateName = target._dispatchInstances.memoizedProps.name;
    var stateValue = target.nativeEvent.text;
    this.setState({
      [stateName]: stateValue,
    });
  }

  handleCheck = async () => {
    this.setState({checkedRedeban: !this.state.checkedRedeban});
    this.setState({checkedPrint: !this.state.checkedPrint});
    if (!this.state.checkedRedeban || !this.state.checkedPrint) {
      await AsyncStorage.setItem('checkedRedeban', 'true');
      await AsyncStorage.setItem('checkedPrint', 'true');
      this.props.navigation.setParams({Display: 'flex'});
    } else {
      await AsyncStorage.setItem('checkedRedeban', 'false');
      await AsyncStorage.setItem('checkedPrint', 'false');
      this.props.navigation.setParams({Display: 'none'});
    }
  };

  handleCheckPrint = async () => {
    this.setState({checkedPrint: !this.state.checkedPrint});
    if (!this.state.checkedPrint) {
      await AsyncStorage.setItem('checkedPrint', 'true');
    } else {
      await AsyncStorage.setItem('checkedPrint', 'false');
    }
  };

  changeText = (value, input) => {
    if (input == 'url') {
      var validateValue = helpers.validateValueUrl(value);
      this.setState({validateUrl: validateValue});
      this.setState({validateShowUrl: !validateValue});
    } else if (input == 'datafono') {
      var validateValue = helpers.validateValueDatafono(value);
      this.setState({onValidateDatafono: validateValue});
    }
  };

  static navigationOptions = ({navigation}) => {
    return {
      title: 'Configuración Redeban',
      headerRight: (
        <View style={{display: navigation.getParam('Display', 'none')}}>
          <TouchableOpacity
            style={styles.btnSave}
            onPress={navigation.getParam('saveData')}>
            <Text style={styles.textSave}>Guardar</Text>
          </TouchableOpacity>
        </View>
      ),
    };
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <CheckBox
            style={styles.checkbox}
            value={this.state.checkedRedeban}
            onValueChange={this.handleCheck}
            checkBoxColor="red"
          />
          <Text style={{marginTop: 15}}>Activar Redeban</Text>
        </View>
        <Text>Url</Text>
        <TextInput
          style={styles.inputCustom}
          name="dataRedeUrl"
          placeholder="https://"
          defaultValue="https://"
          value={this.state.dataRedeUrl}
          onChange={this.handleChange}
          onChangeText={(text) => this.changeText({text}, 'url')}
        />
        {this.state.validateShowUrl && (
          <Text style={styles.msmRequired}>
            <Text style={styles.msmSpanRequired}>*</Text>Verifique que el campo
            si tenga una url valida
          </Text>
        )}

        <Text>Identificación del terminal</Text>
        <TextInput
          style={styles.inputCustom}
          name="dataRedeIdDatafono"
          placeholder="Datáfono"
          value={this.state.dataRedeIdDatafono}
          onChange={this.handleChange}
          dataDetectorTypes="link"
          onChangeText={(text) => this.changeText({text}, 'datafono')}
        />
        {this.state.onValidateDatafono && (
          <Text style={styles.msmRequired}>
            <Text style={styles.msmSpanRequired}>*</Text>Debe ser mayor a 6
            dígitos
          </Text>
        )}

        <Text>Modelo del Datáfono</Text>
        <View style={styles.inputCustom}>
          <Picker
            style={styles.customPicker}
            selectedValue={this.state.dataRedeModel}
            onValueChange={(value) => {
              this.setState({dataRedeModel: value});
              if (value == 'scan') {
                this.setState({scannerPrint: true});
              }
            }}>
            <Picker.Item label="Seleccione un modelo de Datáfono" />
            <Picker.Item label="Lane/500" value="lane500" />
            <Picker.Item label="Move2500" value="move2500" />
            <Picker.Item label="A920" value="a920" />
            <Picker.Item label="Scanner QR" value="scan" />
          </Picker>
        </View>
        {this.state.scannerPrint && (
          <View style={{flexDirection: 'row'}}>
            <CheckBox
              style={styles.checkbox}
              value={this.state.checkedPrint}
              onValueChange={this.handleCheckPrint}
            />
            <Text style={{marginTop: 15}}>Activar Impresora</Text>
          </View>
        )}

        {this.state.onValidate && (
          <Text style={styles.msmRequired}>
            <Text style={styles.msmSpanRequired}>*</Text>Todos los campos son
            requeridos
          </Text>
        )}
      </View>
    );
  }
}
