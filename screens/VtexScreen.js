import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  TextInput,
  ScrollView,
} from 'react-native';
import {styles} from '../assets/styles/styles';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Divider from 'react-native-divider';

export default class VtexScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataIva: null,
      dataPusherId: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.saveData = this.saveData.bind(this);
  }

  componentDidMount() {
    this.handleLoad();
  }

  handleLoad = async () => {
    console.log('In Vtex Settings');
    try {
      const value = await AsyncStorage.getItem('dataIva');
      const dataPusherId = await AsyncStorage.getItem('dataPusherId');
      const dataPusherChannel = await AsyncStorage.getItem('dataPusherChannel');
      const dataPusherEvent = await AsyncStorage.getItem('dataPusherEvent');
      if (value !== null || dataPusherId !== null) {
        this.setState({dataIva: value});
        this.setState({dataPusherId: dataPusherId});
        this.setState({dataPusherChannel: dataPusherChannel});
        this.setState({dataPusherEvent: dataPusherEvent});
      } else {
        console.log('No Data');
      }
    } catch (e) {
      // error reading value
    }
  };

  storeData = async (keySave, valueSave) => {
    try {
      await AsyncStorage.setItem(keySave.toString(), valueSave.toString());
      console.log('Success save data');
    } catch (e) {
      console.log('Error save data');
    }
  };

  saveData = async (keySave, valueSave) => {
    console.log(this.state.dataIva);
    console.log(this.state.dataPusherId);
    console.log(this.state.dataPusherChannel);
    console.log(this.state.dataPusherEvent);

    try {
      await AsyncStorage.setItem('dataIva', this.state.dataIva.toString());
      await AsyncStorage.setItem(
        'dataPusherId',
        this.state.dataPusherId.toString(),
      );
      await AsyncStorage.setItem(
        'dataPusherChannel',
        this.state.dataPusherChannel.toString(),
      );
      await AsyncStorage.setItem(
        'dataPusherEvent',
        this.state.dataPusherEvent.toString(),
      );
      Toast.showWithGravity(
        'Información Redeban guardada correctamente.',
        Toast.LONG,
        Toast.CENTER,
      );
      console.log('Success save data');
    } catch (e) {
      console.log('Error save data');
    }
  };

  handleChange(target) {
    var stateName = target._dispatchInstances.memoizedProps.name;
    var stateValue = target.nativeEvent.text;
    this.setState({
      [stateName]: stateValue,
    });
  }

  static navigationOptions = {
    title: 'Configuración de Vtex',
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text>APP KEY</Text>
          <TextInput style={styles.inputCustom} />

          <Text>APP TOKEN</Text>
          <TextInput style={styles.inputCustom} />

          <Text>Iva %</Text>
          <TextInput
            style={styles.inputCustom}
            keyboardType="numeric"
            name="dataIva"
            value={this.state.dataIva}
            onChange={this.handleChange}
            maxLength={2}
          />

          <Divider color="#F71963" orientation="center">
            PUSHER
          </Divider>
          <Text>Pusher Id</Text>
          <TextInput
            style={styles.inputCustom}
            name="dataPusherId"
            value="e5bc1a82364ef87a73d4"
            onChange={this.handleChange}
          />

          <Text>Pusher Channel</Text>
          <TextInput
            style={styles.inputCustom}
            name="dataPusherChannel"
            value={this.state.dataPusherChannel}
            onChange={this.handleChange}
          />

          <Text>Pusher Event</Text>
          <TextInput
            style={styles.inputCustom}
            name="dataPusherEvent"
            value={this.state.dataPusherEvent}
            onChange={this.handleChange}
          />

          <TouchableOpacity style={styles.btnSave} onPress={this.saveData}>
            <Text style={styles.textSave}>Guardar</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
