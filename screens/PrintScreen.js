import React, { Component } from 'react';
import {Text, View, Linking, TouchableOpacity, NativeModules, StyleSheet } from "react-native";
import * as UrlNode from 'url';

let printEmail='', listProducts = ''
let dataProducts = []
let dataPrint = {}

export default class PrintScreen extends Component {
   constructor(props) {
      super(props);
      this.state = {
      };
      this._handleOpenURL = this._handleOpenURL.bind(this);
   }

   componentDidMount(){
      /** 
       * Validat Initial Url
      */
      Linking.getInitialURL().then((valueUrl) => { this._handleInitialURL(valueUrl) })
      Linking.addEventListener('url', this._handleOpenURL);
   }

   _handleInitialURL(valueUrl) {
      if (valueUrl) {
        this.startProcess(valueUrl)
      }
    }
  
   _handleOpenURL(event) {
      if (event.url) {
         this.startProcess(event.url)
      }
   }

   startProcess(valueUrl){
      const urlParams = UrlNode.parse(valueUrl, true);
      let queryData = urlParams.query

      if (queryData['clientProfileData[firstName]'] === 'isAnonymous') {
         printEmail = printEmail = "anonymous@vtex.com";
      } else {
         printEmail = queryData['clientProfileData[email]'];
      }

      if (queryData['items[0][name]']) {
         let index = 0
         for (var [key, value] of Object.entries(queryData)) {
            if (queryData['items['+index+'][name]']) {
               let uniqueId = queryData['items['+index+'][productId]']

               dataProducts.push(uniqueId = {
                  uniqueId : uniqueId,
                  qty: queryData['items['+index+'][quantity]'],
                  name: queryData['items['+index+'][name]'],
                  price: queryData['items['+index+'][price]'],
                  // price: queryData['items['+index+'][costPrice]'],
               });
            } else {
               break;
            }
            index = index+1
         }
      }
      
      for (let index = 0; index < dataProducts.length; index++) {
         const element = dataProducts[index];
         let nameCustom = element.name
         console.log(nameCustom.length)
         if (nameCustom.length > 30) {
            nameCustom = nameCustom + "\n"
            console.log('other', nameCustom)
         }
         listProducts += "("+element.qty +") "+ nameCustom +" "+ element.price + "\n"
      }

      dataPrint = {
         storeName: queryData['marketplace[name]'],
         storeNit: "900261429-1",
         storeDir: "Medellín - Tel / 7428030",
         listProducts: listProducts,
         email: printEmail,
         name: queryData['clientProfileData[firstName]'],
         document: queryData['clientProfileData[document]'],
         total: queryData['paymentData[transactions][0][payments][0][value]'].toString(),
         paymentType: queryData['paymentData[transactions][0][payments][0][paymentSystemName]'],
         storeInfo: "www.store.sony.com.co  Nit:900261429-1",
         vendor: queryData['callCenterOperatorData[userName]'],
         makeBy: "SONY / POS"
      }

      console.log(dataPrint)
   }
   

   _onPrint() {
      let date = new Date().getDate();
      let month = new Date().getMonth() + 1;
      let year = new Date().getFullYear();

      let currentDate = date+'/'+month+'/'+year
      let time = new Date().getHours()+':'+new Date().getMinutes()

      try {
         NativeModules.ActivityStarter.imprimir(
            dataPrint.storeName, 
            dataPrint.storeNit, 
            dataPrint.storeDir,
            "19816549/8156",
            "Recibo de Caja Ventas Por Mostrador",
            dataPrint.listProducts,
            dataPrint.email,
            currentDate,
            time,
            dataPrint.name,
            dataPrint.document,
            dataPrint.total,
            dataPrint.paymentType,
            dataPrint.storeInfo,
            dataPrint.vendor,
         );

         // NativeModules.ActivityStarter.imprimir(
         //    "Sony Colombia S.a.", 
         //    "900261429-1", 
         //    "Medellín - Tel / 7428030",
         //    "19816549/8156",
         //    "Recibo de Caja Ventas Por Mostrador",
         //    "01235345",
         //    printEmail,
         //    currentDate,
         //    time,
         //    "Adrian",
         //    "123123",
         //    printTotal.toString(),
         //    "Efectivo",
         //    "www.store.sony.com.co  Nit:900261429-1",
         //    "SONY / POS",
         // );
      } catch (error) {
         console.log('Error 1')
      } 
   }

  static navigationOptions = { header: null };
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.btn} onPress={ this._onPrint }>
            <Text style={styles.paragraph}>Imprimir Factura</Text>
         </TouchableOpacity>
      </View>
    );
  }
}

let styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   btn: {
      textAlign: 'center',
      padding: 20,
      backgroundColor: '#F71963',
      padding: 20,
      borderRadius: 5
   },
   paragraph: {
      fontFamily: 'Fabriga',
      textAlign: 'center',
      color: '#FFFFFF',
      fontSize: 20,
   },
 });