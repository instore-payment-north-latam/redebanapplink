import React, {Component} from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  Linking,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as UrlNode from 'url';
import DeviceInfo from 'react-native-device-info';
import helpers from '../components/helpers';

let registryModel;
let renHome;
global.urlGlobal = 'url';

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      android5: false, //if android version is 5, show additional button in screen
    };

    this.webView = null;
    this.onMessage = this.onMessage.bind(this);
    this._handleOpenURL = this._handleOpenURL.bind(this);
  }

  componentDidMount() {
    this.getInitalData();
    this.setState({version: DeviceInfo.getVersion()});

    /**
     * Validate System compatibility
     */
    let getSystemName = DeviceInfo.getSystemName();
    let getSystemVersion = DeviceInfo.getSystemVersion();
    if (getSystemName === 'Android' && getSystemVersion.includes('5.1')) {
      this.setState({android5: true});
    }

    /**
     * Validat Initial Url
     */
    Linking.getInitialURL().then((valueUrl) => {
      this._handleInitialURL(valueUrl);
    });
    Linking.addEventListener('url', this._handleOpenURL);
  }

  _handleInitialURL(valueUrl) {
    if (valueUrl) {
      this.startProcess(valueUrl);
    }
  }

  _handleOpenURL(event) {
    if (event.url) {
      this.startProcess(event.url);
    }
  }

  startProcess(valueUrl) {
    urlGlobal = valueUrl;
    const urlParams = UrlNode.parse(valueUrl, true);
    this.renderFunction(urlParams);
  }

  renderFunction(urlParams) {
    const {navigate} = this.props.navigation;

    /**
     * Validat device type, if is A920 or A910 go to NewPayment screen
     * NewPayment: process all payments with redebna integrations A910, A920
     * PayTransaction: process order for devices like Lane5000...
     */
    if (urlParams.query.screen === 'PrintScreen') {
      navigate('PrintScreen');
    } else if (urlParams.query.action === 'payment') {
      DeviceInfo.getDeviceName().then((deviceName) => {
        if (
          deviceName.indexOf('A920') > -1 ||
          deviceName.indexOf('A910') > -1
        ) {
          navigate('NewPayment');
        } else {
          navigate('PayTransaction');
        }
      });
    }
  }

  rendering(urlParams) {
    renHome = urlParams.query.home;
    if (!renHome) {
      const {navigate} = this.props.navigation;
      navigate('ProcessingScreen');
    }
  }

  /**
   * Validate inital configuration, this is necessary to be able to record transactions
   * To view how configure device read README links
   */
  getInitalData = async () => {
    try {
      registryModel = await AsyncStorage.getItem('registryModel');
      if (registryModel) {
        this.sendDataToWebView();
      } else {
        Alert.alert('Falta configuración inicial');
      }
    } catch (error) {
      console.log('Error in getInitalData');
    }
  };

  /**
   * OnMessage is the communication between react nativa app and reac app (Webview)
   */
  onMessage(event) {
    let {navigate} = this.props.navigation;
    let data = event.nativeEvent.data;
    if (data === 'scanner') {
      navigate('NewScannerScreen');
    } else {
      let dataArray = data.split('&');
      for (let userObject of dataArray) {
        let dataStore = userObject.split('=');
        if (dataStore[1]) {
          this.storeData(dataStore[0], dataStore[1]);
        }
      }
    }
  }

  /**
   * Save data in device storage
   */
  storeData = async (keySave, valueSave) => {
    try {
      await AsyncStorage.setItem(keySave.toString(), valueSave.toString());
      console.log('Success save data');
    } catch (e) {
      console.log('Error save data');
    }
  };

  renderLoadingView() {
    return <ActivityIndicator size="large" color="#F71963" />;
  }

  webLoading = async () => {
    try {
      registryModel = await AsyncStorage.getItem('registryModel');
      if (registryModel) {
        this.sendDataToWebView();
      }
    } catch (error) {
      console.log('Error in get AsyncStorage');
    }
  };

  sendDataToWebView() {
    let data = {
      registryModel: registryModel,
    };
    this.webView.postMessage(JSON.stringify(data));
  }

  static navigationOptions = {header: null};

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex: 1}}>
        <WebView
          source={{uri: helpers.urlSurge}}
          ref={(webView) => (this.webView = webView)}
          onMessage={this.onMessage}
          renderLoading={this.renderLoadingView}
          startInLoadingState={true}
          onLoadEnd={() => this.webLoading()}
          androidLayerType="hardware"
        />
        {this.state.android5 && (
          <TouchableOpacity
            style={{padding: 10, backgroundColor: '#142032'}}
            onPress={() => navigate('ConfigDevice')}>
            <Text style={{color: 'white', textAlign: 'center'}}>
              Configuración
            </Text>
          </TouchableOpacity>
        )}
        <Text style={{textAlign: 'right'}}>version {this.state.version} </Text>
      </View>
    );
  }
}
