import React, {Component} from "react";
import {AppRegistry, Text, View, TouchableHighlight, TouchableOpacity,
    NativeModules,
    StyleSheet
} from "react-native";
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import WS from 'react-native-websocket'


export default class PusherScreen extends Component {

    constructor( props ) {
        super( props );
    }

    static navigationOptions = { header: null };

    send = (data) => this.state.ws.send(data)
    componentDidMount () {
        
        this.reconnect = !!this.props.reconnect
        this._handleWebSocketSetup()
    }
    componentWillUnmount () {
        this.reconnect = false
        this.state.ws.close()
    }
    _handleWebSocketSetup = () => {
        const ws = new WebSocket('wss://pubsub.vtexnorthlatam.com:9850/sub?id=e0520f8ac2e1e1bc385cf693a5364965b66198e65a3db5ab8060914ff41e269e')
        ws.onopen = (e) => {
            console.log('OPEN', e)
            this.props.onOpen && this.props.onOpen()
        }
        ws.onmessage = (event) => { 
            console.log('Message from websocket')
            console.log(event)
            this.props.onMessage && this.props.onMessage(event) 
        }
        ws.onerror = (error) => { 
            console.log('error')
            this.props.onError && this.props.onError(error) 
        }
        ws.onclose = () => this.reconnect ? this._handleWebSocketSetup() : (this.props.onClose && this.props.onClose())
        this.setState({ws})
    }

    print() {
        console.log('Imprimiendo');
    }

    _onPrint() {
        console.log('Imprimiendo2');

        try {
            NativeModules.ActivityStarter.imprimir(
                "VTEX S.A.S", 
                "900.342.234-2", 
                "Carrera 45",
                "nombre de la tienda",
                "Recibo de Caja Ventas Por Mostrador",
                "01235345",
                "No Maquina",
                "09/10/18",
                "18:56",
                "Adrian",
                "123123",
                "120.000",
                "ScannerQR",
                "www.vtex.com/co-es/",
                "Vendedor"
                );
            console.log('YES!');
        } catch (error) {
            console.log('Error 1')
        } 
    }

    

    viewData() {
        

        let dataUniqueId = DeviceInfo.getUniqueId()
        let dataModel = DeviceInfo.getModel()
        let dataSystemName = DeviceInfo.getSystemName()
        let dataBrand = DeviceInfo.getBrand()
        let dataSystemVersion = DeviceInfo.getSystemVersion()
        let dataDeviceType = DeviceInfo.getDeviceType()

        let dataDevice = {
           "deviceID": dataUniqueId,
           "deviceType": dataDeviceType,
           "deviceModel": dataModel,
           "dataBrand": dataBrand,
           "os": dataSystemName,
           "osVersion": dataSystemVersion
        }

        let useUrl = 'https://pubsub.vtexnorthlatam.com:9850/pub?id=e0520f8ac2e1e1bc385cf693a5364965b66198e65a3db5ab8060914ff41e269e'
        console.log(dataDevice, useUrl)

     
        axios.post(
            useUrl, 
            dataDevice
        ).then((response) => {
            console.log(response.data);
        })

        // https://vtexadrian--newbusinessunits.myvtex.com/_rappi/getId
        // useEffect(() => {
            //     axios ({
            //         method: 'GET',
            //         url: location.origin+'/_rappi/getId',
            //      }).then(res => {
            //         var vbaseData = res.data
            //         if (vbaseData.vtex_affiliate_id) {
            //             setVtex_enviroment(vbaseData.vtex_enviroment)
            //             setVtex_rappi_account(vbaseData.vtex_rappi_account)
            //             setVtex_affiliate_id(vbaseData.vtex_affiliate_id)
            //             setVtex_sales_channel(vbaseData.vtex_sales_channel)
            //             setVtex_app_key(vbaseData.vtex_app_key)
            //             setVtex_app_token(vbaseData.vtex_app_token)
            //         }
            //      })
            // }, []);
    }

    render() {
        return (
            <View style={styles.container}>


                {/* <WS
                    ref={ref => {this.ws = ref}}
                    url="wss://echo.websocket.org/"
                    onOpen={() => {
                    console.log('Open!')
                    
                    }}
                    onMessage={console.log('Open1!')}
                    onError={console.log('Open2!')}
                    onClose={console.log('Open3!')}
                    reconnect // Will try to reconnect onClose
                /> */}

                {/* <TouchableOpacity
                    onPress={ this._onPrint }>
                    <Text style={styles.hello}>PRINTs</Text>
                </TouchableOpacity> */}


                <TouchableOpacity
                    onPress={ this.viewData }>
                    <Text style={styles.hello}>Ver data</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center'
    },
    hello: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10
    }
  });