import React, {Component} from 'react';
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Picker,
} from 'react-native';
import Collapsible from 'react-native-collapsible';
import {styles} from '../assets/styles/styles';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Divider from 'react-native-divider';

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      showElement: false,
      collapsed: true,
      collapsedMp: true,
      dataNameStore: null,
      dataValueCountry: null,
      dataIva: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.sendForm = this.sendForm.bind(this);
  }

  componentDidMount() {
    this.handleLoad();
    this.props.navigation.setParams({sendForm: this.sendForm});
  }

  handleLoad = async () => {
    try {
      const storeName = await AsyncStorage.getItem('storeName');
      const dataValueCountry = await AsyncStorage.getItem('dataValueCountry');
      if (storeName !== null && dataValueCountry !== null) {
        this.setState({dataNameStore: storeName});
        this.setState({dataValueCountry: dataValueCountry});
        this.setState({dataIva: dataIva});

        if (dataValueCountry == 'colombia') {
          this.setState({showElement: true});
        }
      }
    } catch (error) {
      console.log('No found eleement in storage');
    }
  };

  handleChange(target) {
    var stateName = target._dispatchInstances.memoizedProps.name;
    var stateValue = target.nativeEvent.text;
    this.setState({
      [stateName]: stateValue,
    });
  }

  toggleExpanded = () => {
    this.setState({collapsed: !this.state.collapsed});
  };

  toggleExpandedMp = () => {
    this.setState({collapsedMp: !this.state.collapsedMp});
  };

  sendForm = async () => {
    console.log('sendForm');
    var dataNameStore = this.state.dataNameStore;
    var dataValueCountry = this.state.dataValueCountry;
    var dataIva = this.state.dataIva;

    console.log(dataNameStore, dataValueCountry, dataIva);
    if (dataNameStore && dataValueCountry && dataIva) {
      console.log('yes');
      this.setState({onValidate: false});
      try {
        await AsyncStorage.setItem('dataNameStore', this.state.dataNameStore);
        await AsyncStorage.setItem(
          'dataValueCountry',
          this.state.dataValueCountry,
        );
        await AsyncStorage.setItem('dataIva', this.state.dataIva);
        Toast.showWithGravity(
          'Información guardada correctamente.',
          Toast.LONG,
          Toast.CENTER,
        );
      } catch (error) {
        alert('error');
      }
    } else {
      this.setState({onValidate: true});
    }
  };

  static navigationOptions = ({navigation}) => ({
    title: 'Configuración',
    headerRight: (
      <TouchableOpacity
        style={styles.btnSave}
        onPress={navigation.getParam('sendForm')}>
        <Text style={styles.textSave}>Guardar</Text>
      </TouchableOpacity>
    ),
  });

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <ScrollView>
          <Divider color="#F71963" orientation="center">
            GENERAL
          </Divider>
          <Text>Nombre de la tienda</Text>
          <TextInput
            style={styles.inputCustom}
            name="dataNameStore"
            value={this.state.dataNameStore}
            onChange={this.handleChange}
          />
          <View>
            {this.state.onValidate && (
              <Text style={styles.msmRequired}>
                <Text style={styles.msmSpanRequired}>*</Text>Este campo es
                requerido
              </Text>
            )}
          </View>

          <Text>País</Text>
          <View style={styles.inputCustom}>
            <Picker
              name="dataValueCountry"
              style={styles.customPicker}
              selectedValue={this.state.dataValueCountry}
              onValueChange={(value) => {
                this.setState({dataValueCountry: value});
              }}>
              <Picker.Item label="Seleccione un país" />
              <Picker.Item label="Colombia" value="colombia" />
              <Picker.Item label="Ecuador" value="ecuador" />
              <Picker.Item label="Perú" value="peru" />
              <Picker.Item label="Panamá" value="panama" />
            </Picker>
          </View>
          <View>
            {this.state.onValidate && (
              <Text style={styles.msmRequired}>
                <Text style={styles.msmSpanRequired}>*</Text>Este campo es
                requerido
              </Text>
            )}
          </View>

          <Text>Iva</Text>
          <TextInput
            style={styles.inputCustom}
            name="dataIva"
            value={this.state.dataIva}
            onChange={this.handleChange}
          />
        </ScrollView>
      </View>
    );
  }
}

export default SettingsScreen;
