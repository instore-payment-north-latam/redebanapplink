import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Linking,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import * as UrlNode from 'url';
import {approvedStyle, globalStyle} from '../assets/styles/styles';
import {buildResponseUrl, buildFailedResponseUrl} from '../utils';
import RNExitApp from 'react-native-exit-app';
import {clone} from 'ramda';

let codeID;
export default class WebservisePay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlIncoming: false,
      value: false,
      transactionError: false,
      startTransaction: false,
      approvedPay: false,
      txtError: '',
    };

    this.createTransaction = this.createTransaction.bind(this);
    this._returnFailedUrl = this._returnFailedUrl.bind(this);
  }

  send = (data) => this.state.ws.send(data);
  componentDidMount() {
    this.intialConfig();
  }

  _handleWebSocketSetup = () => {
    const ws = new WebSocket(
      'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + codeID,
    );
    ws.onopen = (e) => {
      console.log('OPEN', e);
      this.props.onOpen && this.props.onOpen();
    };
    ws.onmessage = (event) => {
      console.log('Message from websocket');
      console.log(event);
      this.props.onMessage && this.props.onMessage(event);
    };
    ws.onerror = (error) => {
      console.log('error');
      this.props.onError && this.props.onError(error);
    };
    ws.onclose = () =>
      this.reconnect
        ? this._handleWebSocketSetup()
        : this.props.onClose && this.props.onClose();
    this.setState({ws});
  };

  intialConfig = async () => {
    let transactionDeviceId;
    try {
      transactionDeviceId = await AsyncStorage.getItem('transactionDeviceId');
      // let transaction_id = await AsyncStorage.getItem('transaction_id');
      if (transactionDeviceId) {
        this.createTransaction(transactionDeviceId);
      }
    } catch (e) {
      console.log('Error in get AsyncStorage');
    }
  };

  createTransaction(transactionDeviceId) {
    this.setState({transactionError: true});
    this.setState({transactionError: false});
    const urlParams = UrlNode.parse(urlGlobal, true);

    let infoTransaction = {
      orderId: urlParams.query.orderGroupId, // orderGroupId
      paymentId: urlParams.query.paymentId, // paymentId
      transactionId: urlParams.query.transactionId, // transactionId
      deviceId: transactionDeviceId, // ID del documento del dispositivo en la DB
      paymentType: 'webservice-redeban-pay', // "webservice-redeban-pay" o "deeplinking-redeban"
      payerEmail: urlParams.query.payerEmail, // payerEmail
      payerIdentification: '12345678909',
      amount: urlParams.query.value, // (value / 100)
      taxRate: 0.19,
      debugMode: true, // Pendiente
    };
    console.log('infoTransaction', infoTransaction);

    // let infoTransaction = {
    //   "orderId": "0202_48",
    //   "paymentId": "0202_48",
    //   "transactionId": "0202_48",
    //   "deviceId": transactionDeviceId,
    //   "paymentType": "webservice-redeban-pay",
    //   "payerEmail": "customeremail@gmail.com",
    //   "payerIdentification": "12345678909",
    //   "amount": 1000,
    //   "taxRate": 0.19,
    //   "debugMode": true
    // }

    let config = {
      method: 'post',
      url: 'https://transactions.vtexnorthlatam.com/api/transactions',
      headers: {
        'Content-Type': 'application/json',
      },
      data: infoTransaction,
    };

    axios(config)
      .then((response) => {
        let data = response.data.data;
        console.log(data._id);
        if (data._id) {
          codeID = data._id;

          let useUrl = 'wss://pubsub.vtexnorthlatam.com:9850/sub?id=' + codeID;
          const ws = new WebSocket(useUrl);
          ws.onmessage = (event) => {
            console.log('Response WebSocket:', event);
            let data = JSON.parse(event.data);

            if (data.status == 'rejected') {
              this.setState({transactionError: true});
              this.setState({txtError: 'Estado RECHAZADO'});
            } else if (data.status == 'approved') {
              this.setState({approvedPay: true});
              console.log('is approved');
            }
            this.props.onMessage && this.props.onMessage(event);
          };
          this.setState({startTransaction: true});
          this.setState({ws});
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  saveDataStore = async (keySave, valueSave) => {
    console.log('Save Storage Info: ', keySave, valueSave);
    try {
      await AsyncStorage.setItem(keySave.toString(), valueSave.toString());
      console.log('Success save data');
    } catch (e) {
      console.log('Error save data');
    }
  };

  _returnFailedUrl() {
    var textError = this.state.txtError;
    Linking.openURL(buildFailedResponseUrl('10', textError.toString()));
    RNExitApp.exitApp();
  }

  _returnUrl() {
    const tempUrl = clone(urlGlobal);

    Linking.openURL(buildResponseUrl(tempUrl, '234', 'test', '123'));
    RNExitApp.exitApp();
  }

  static navigationOptions = {header: null};
  render() {
    return (
      <View style={styles.content}>
        {!this.state.transactionError &&
          this.state.startTransaction &&
          !this.state.approvedPay && (
            <View>
              <Image
                source={require('../assets/images/pay.gif')}
                style={{width: 360, height: 360}}
              />
              <Text style={styles.title}>Procesando pago</Text>
            </View>
          )}

        {this.state.transactionError && (
          <View>
            <Text style={[globalStyle.textCenter, globalStyle.paragraph]}>
              Mensaje: {this.state.txtError}
            </Text>
            <View style={approvedStyle.contentButtons}>
              <TouchableOpacity
                style={[approvedStyle.buttons, approvedStyle.cancelButton]}
                onPress={this._returnFailedUrl}>
                <Text style={[globalStyle.centerWhite, globalStyle.paragraph]}>
                  Cancelar
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={approvedStyle.buttons}
                onPress={this.createTransaction}>
                <Text style={[globalStyle.centerWhite, globalStyle.paragraph]}>
                  Reintentar
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        {this.state.approvedPay && (
          <View>
            <Text style={[globalStyle.textCenter, globalStyle.paragraph]}>
              PAGO EXITOSO
            </Text>

            <View style={approvedStyle.contentButtons}>
              <TouchableOpacity
                style={[approvedStyle.buttons, approvedStyle.cancelButton]}
                onPress={this._returnFailedUrl}>
                <Text style={[globalStyle.centerWhite, globalStyle.paragraph]}>
                  Cancelar
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={approvedStyle.buttons}
                onPress={this._returnUrl}>
                <Text style={[globalStyle.centerWhite, globalStyle.paragraph]}>
                  Continuar
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Fabriga',
    fontSize: 20,
    textAlign: 'center',
  },
});
