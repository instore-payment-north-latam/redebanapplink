import React, {Component} from 'react';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from '@apollo/react-hooks';
import PaymentProcessing from '../PaymentProcessing';
import WebservicePay from './WebservicePay';
import NewPayment from './NewPayment';
import ScanScreen from './ScanScreen';
import {
  TouchableOpacity,
  Text,
  Button,
  Linking,
  View,
  StyleSheet,
} from 'react-native';
import {styles, globalStyle} from '../assets/styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info';

let isA920 = false;
const client = new ApolloClient({
  uri: 'https://trx.vtxppp.com/graphql',
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleDialog: false,
      modelDatafono: true,
    };

    this.reRender = this.props.navigation.addListener('willFocus', () => {
      this.handleLoad();
    });

    this.handleLoad = this.handleLoad.bind(this);
  }

  componentDidMount() {
    this.handleLoad();
    // DeviceInfo.getDeviceName().then(deviceName => {
    //    console.log(deviceName);
    //    if (deviceName.indexOf('A920') > -1) {
    //       // isA920 = true;
    //    }
    // });
  }

  handleLoad = async () => {
    try {
      const value = await AsyncStorage.getItem('dataRedeModel');
      const setPayMode = await AsyncStorage.getItem('setPayMode');

      console.log('value:', value);
      console.log('setPayMode:', setPayMode);
      if (isA920) {
        console.log('Device is A920');
        this.setState({modelDatafono: 'deepLinking'});
      } else {
        if (setPayMode == 'lane') {
          this.setState({modelDatafono: 'appLinking'});
        } else if (setPayMode == 'redeban') {
          this.setState({modelDatafono: 'deepLinking'});
        } else if (setPayMode == 'scanner') {
          this.setState({modelDatafono: 'scan'});
        }
      }
    } catch (error) {
      alert('Error');
    }
  };

  static navigationOptions = ({navigation}) => ({
    headerLeft: (
      <TouchableOpacity
        style={globalStyle.headerSides}
        onPress={() => navigation.navigate('HomeScreen')}>
        <Icon name="md-home" color="#142032" size={35} />
      </TouchableOpacity>
    ),
  });
  static navigationOptions = {header: null};

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={localStyle.content}>
        {this.state.modelDatafono == 'appLinking' && <WebservicePay />}
        {this.state.modelDatafono == 'deepLinking' && (
          <NewPayment navigation={this.props.navigation} />
        )}
        {this.state.modelDatafono == 'scan' && (
          <ScanScreen navigation={this.props.navigation} />
        )}
      </View>
    );
  }
}

const localStyle = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Fabriga',
    fontSize: 20,
    textAlign: 'center',
  },
});
