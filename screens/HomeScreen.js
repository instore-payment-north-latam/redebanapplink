import React, {Component} from 'react';
import {
  View,
  Text,
  Linking,
  TouchableOpacity,
  ActivityIndicator,
  AppState,
} from 'react-native';
import {globalStyle, styles} from '../assets/styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as UrlNode from 'url';
import {WebView} from 'react-native-webview';
import RNExitApp from 'react-native-exit-app';

var renHome;
class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showElement: false,
      appState: AppState.currentState,
    };

    this.reRender = this.props.navigation.addListener('willFocus', () => {
      this.handleLoad();
    });

    this.onMessage = this.onMessage.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
  }

  /**
   * Init function when screen charge
   */
  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    this.handleLoad();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('- App has come to the foreground!');
    }
    this.setState({appState: nextAppState});
  };

  /**
   * Validate if Storage contains settings values
   * */
  handleLoad = async () => {
    console.log('In Home Screen');

    Linking.getInitialURL()
      .then(_handleInitialUrl => {
        if (_handleInitialUrl) {
          url = _handleInitialUrl;
          console.log('Initial url is: ' + _handleInitialUrl);
          const urlParams = UrlNode.parse(_handleInitialUrl, true);

          var paymentSystemName = urlParams.query.paymentSystemName;
          console.log('paymentSystemName: ', paymentSystemName);
          if (paymentSystemName == 'Venda Direta Debito') {
            console.log('HEYYYY');
            this.setPayMode();
          }
          renHome = urlParams.query.home;
          console.log('Home renHome', renHome);
          if (renHome) {
            console.log(renHome);
          } else {
            const {navigate} = this.props.navigation;
            navigate('ProcessingScreen');
          }
        } else {
          console.log('Error with Initial Url');
        }
      })
      .catch(err => console.error('An error occurred', err));

    try {
      const value = await AsyncStorage.getItem('storeName');
      if (value !== null) {
        this.setState({showElement: false});
      } else {
        this.setState({showElement: true});
      }
    } catch (error) {
      alert('Error');
    }
  };

  setPayMode = async () => {
    try {
      await AsyncStorage.setItem('setPayMode', 'scanner');
      console.log('Success save data');
    } catch (e) {
      console.log('Error save data');
    }
  };

  sendUrl() {
    console.log('object');
    // Linking.openURL('instore://cart-change/?orderFormId=0963e4fa3bf94f2893a21e8fe6d4a0ca&forceIdentification=false&isCheckedIn=true&next=payment').catch((err) => console.error('An error occurred', err));
  }

  static navigationOptions = ({navigation}) => ({
    headerLeft: (
      <TouchableOpacity
        style={globalStyle.headerSides}
        onPress={() => navigation.navigate('NewSettingsScreen')}>
        <Icon name="ios-settings" color="#142032" size={35} />
      </TouchableOpacity>
    ),
    headerRight: (
      <TouchableOpacity
        style={globalStyle.headerSides}
        onPress={() => navigation.navigate('ProcessingScreen')}>
        <Icon name="ios-albums" color="#142032" size={35} />
      </TouchableOpacity>
    ),
  });

  renderLoadingView() {
    return <ActivityIndicator size="large" color="#F71963" />;
  }

  onMessage(event) {
    var data = event.nativeEvent.data;
    console.log('On Message', data);
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={globalStyle.container}>
        {/* <TouchableOpacity
          style={globalStyle.headerSides}
          onPress={() => navigate('PusherScreen')}>
          <Text>WEBVIEW</Text>
        </TouchableOpacity> */}

        {this.state.showElement && (
          <TouchableOpacity
            style={styles.requiredSetting}
            onPress={() => navigate('SettingsScreen')}>
            <Text style={styles.textAlert}>
              ¡Necesitas una configuración inicial
            </Text>
          </TouchableOpacity>
        )}

        {/* <TouchableOpacity
            onPress={this.sendUrl} >
            <Image style={globalStyle.logoVtex} source={require('../assets/images/vtex-logo.png')} />
        </TouchableOpacity> */}

        <WebView
          source={{uri: 'https://cd9b2150.ngrok.io'}}
          ref={webView => (this.webView = webView)}
          onMessage={this.onMessage}
          renderLoading={this.renderLoadingView}
          startInLoadingState={true}
        />
      </View>
    );
  }
}

export default HomeScreen;
