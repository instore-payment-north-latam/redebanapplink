import React, {Component} from 'react';
import {Linking, ActivityIndicator} from 'react-native';
import {WebView} from 'react-native-webview';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {buildResponseUrl, buildFailedResponseUrl} from '../utils';
import {clone} from 'ramda';
import helpers from '../components/helpers';

export default class PayTransaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: helpers.urlSurge + '/payTransaction' + '?' + new Date(),
      key: 0,
    };

    this.onMessage = this.onMessage.bind(this);
  }

  componentDidMount() {
    console.log('componentDidMount', urlGlobal);
  }

  webLoading() {
    console.log('loadComplete');
  }

  onMessage(event) {
    let data = event.nativeEvent.data;
    // This parameter -data- como to ApplinkWebView, view README
    const defineData = data.split('-data-');
    if (data == 'cancelTransaction') {
      this._returnFailedUrl();
    }
    if (
      data == 'approvedTransaction' ||
      defineData[0] === 'approvedTransaction'
    ) {
      this._returnUrl(defineData[1] || '{}');
    }
    if (data == 'retryTransaction') {
      this.webLoading();
    }
    if (data == 'initTransaction') {
      this.sendData();
    }
  }

  /**
   * Set urlGlobal empty to prevent repeat transaction
   * Go Home screen
   */
  renderHome = async () => {
    urlGlobal = '';
    const {navigate} = this.props.navigation;
    navigate('Index');
  };

  _returnFailedUrl() {
    Linking.openURL(buildFailedResponseUrl('10', 'Transacción cancelada')).then(
      () => {
        this.renderHome();
      },
    );
  }

  _returnUrl(dataTransaction) {
    const convertData = JSON.parse(dataTransaction);
    console.log('PayTransaction -> _returnUrl -> convertData', convertData);
    const tempUrl = clone(urlGlobal);
    Linking.openURL(
      buildResponseUrl(
        tempUrl,
        convertData?.paymentResponse?.response?.cardNumbers || '0000',
        convertData?.paymentResponse?.response?.franchise || 'No cardName',
        convertData?.paymentResponse?.response?.approveCode || '0000',
      ),
    ).then(() => {
      this.renderHome();
    });
  }

  renderLoadingView() {
    return <ActivityIndicator size="large" color="#134cd8" />;
  }

  sendData = async () => {
    let transactionDeviceId;

    try {
      transactionDeviceId = await AsyncStorage.getItem('transactionDeviceId');
      let iva;
      try {
        const getIva = await AsyncStorage.getItem('dataIva');
        console.log('PayTransaction -> sendData -> getIva', getIva);
        if (getIva !== null) {
          iva = getIva;
        } else {
          iva = 19;
        }
      } catch {
        iva = 19;
      }

      if (transactionDeviceId) {
        this.createTransaction(transactionDeviceId, iva);
      }
    } catch (error) {
      console.error(error);
    }
  };

  createTransaction(transactionDeviceId, iva) {
    const infoTransaction = {
      infoUrl: urlGlobal,
      infoDeviceId: transactionDeviceId,
      infoIva: iva / 100,
    };
    console.log('infoTransaction', infoTransaction);
    this.webView.postMessage(JSON.stringify(infoTransaction));
  }

  static navigationOptions = {header: null};
  render() {
    return (
      <>
        <WebView
          source={{uri: this.state.url}}
          ref={(webView) => (this.webView = webView)}
          onMessage={this.onMessage}
          renderLoading={this.renderLoadingView}
          startInLoadingState={true}
          onLoadEnd={() => this.webLoading()}
          androidLayerType="hardware"
          style={{flex: 1}}
          key={this.state.key}
        />
      </>
    );
  }
}
