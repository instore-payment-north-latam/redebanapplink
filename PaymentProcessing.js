/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useCallback, Component} from 'react';
import {Linking, StyleSheet, Text, View, Liking, TouchableOpacity, Button} from 'react-native';
import Spinner from 'react-native-spinkit';
import {useMutation, useLazyQuery} from '@apollo/react-hooks';
import * as UrlNode from 'url';
import {clone} from 'ramda';
import {buildResponseUrl, buildFailedResponseUrl} from './utils';
import {CREATE_TRANSACTION, GET_PAYMENT_BY_RECEIPT} from './graphql';
import {globalStyle} from './assets/styles/styles';
import ApprovedScreen from './screens/ApprovedScreen'

const PaymentProcessingHook = ({navigation, conditionReturn}) => { 
  const [dialog, setDialog] = useState(false);

  const onCancel = () =>{
    console.log('cancel');
  };

  const onInvalid = () =>{
    console.log('Invalid');
  };

  const [url, setUrl] = useState(null);
  const [isWaitingTransaction, setWaitingTransaction] = useState(false);
  const [isTransactioning, setIsTransactioning] = useState(false);
  const [waitTime, setWaitTime] = useState(15000);
  const [tryNumber, setTry] = useState(1);
  const [attempt, setAttempt] = useState(0);

  const [createTransaction, {data: createdTransaction}] = useMutation(
    CREATE_TRANSACTION,
  );
  const [getPaymentByReceipt, {data: paymentReceipt}] = useLazyQuery(
    GET_PAYMENT_BY_RECEIPT,
    {
      fetchPolicy: 'network-only',
    },
  );

  const buildUrlReturn = () =>{
      setWaitingTransaction(false);
      setWaitTime(15000);
      setTry(1);
      setIsTransactioning(false);
      const tempUrl = clone(url);
      setUrl(null);
      Linking.openURL(
        buildResponseUrl(
          tempUrl,
          paymentReceipt.getPaymentByReceipt.response.cardNumbers,
          paymentReceipt.getPaymentByReceipt.response.franchise,
          paymentReceipt.getPaymentByReceipt.response.approveCode,
        ),
      );
  };

  useEffect(() => {
    console.log(
      'TransactionStatus: ',
      paymentReceipt && paymentReceipt.getPaymentByReceipt,
    );
    if (
      isTransactioning &&
      paymentReceipt &&
      paymentReceipt.getPaymentByReceipt &&
      paymentReceipt.getPaymentByReceipt.status === 'pending'
    ) {
      if (tryNumber === 1) {
        setWaitTime(15000);
        setTry(tryNumber + 1);
      } else if (tryNumber === 2) {
        setWaitTime(15000);
        setTry(tryNumber + 1);
      } else {
        setTry(tryNumber + 1);
      }
    } else if (
      isTransactioning &&
      paymentReceipt &&
      paymentReceipt.getPaymentByReceipt &&
      paymentReceipt.getPaymentByReceipt.status === 'approved'
    ) {
      console.log('transaction approved');
      setWaitingTransaction(false);
      setWaitTime(15000);
      setTry(1);
      setIsTransactioning(false);
      const tempUrl = clone(url);
      setUrl(null);

      setDialog(true);

      // Linking.openURL(
      //   buildResponseUrl(
      //     tempUrl,
      //     paymentReceipt.getPaymentByReceipt.response.cardNumbers,
      //     paymentReceipt.getPaymentByReceipt.response.franchise,
      //     paymentReceipt.getPaymentByReceipt.response.approveCode,
      //   ),
      // );
    
    } else if (
      isTransactioning &&
      paymentReceipt &&
      paymentReceipt.getPaymentByReceipt &&
      paymentReceipt.getPaymentByReceipt.status === 'declined'
    ) {
      console.log('transaction declined');
      setWaitingTransaction(false);
      setWaitTime(15000);
      setTry(1);
      setIsTransactioning(false);
      buildFailedResponseUrl(
        createdTransaction.createTransaction.paymentId,
        'timeout',
      );
      setUrl(null);
    }
  }, [paymentReceipt, attempt]);

  useEffect(() => {
    console.log(
      'createdTransaction ',
      createdTransaction && createdTransaction.createTransaction,
    );
    if (
      isTransactioning &&
      createdTransaction &&
      createdTransaction.createTransaction &&
      createdTransaction.createTransaction.status === 'pending' &&
      !isWaitingTransaction
    ) {
      console.log('changing Waiting status ', isWaitingTransaction);
      setWaitingTransaction(true);
    }
  }, [createdTransaction]);

  useEffect(() => {
    if (isTransactioning && isWaitingTransaction) {
      console.log('waiting user...');
      setTimeout(() => {
        console.log(`Trying get transaction after ${waitTime} ms`);
        getPaymentByReceipt({
          variables: {
            paymentId: createdTransaction.createTransaction.paymentId,
          },
        });
        setAttempt(attempt + 1);
      }, waitTime);
    }
  }, [isWaitingTransaction, waitTime, tryNumber]);

  const sendTransaction = useCallback(() => {
    const urlParams = UrlNode.parse(url, true);

    const paymentId = urlParams.query.paymentId;
    const sellerName = urlParams.query.sellerName;
    const orderId = urlParams.query.orderGroupId;
    const amount = urlParams.query.value;
    const truncateAmount = parseFloat(amount.substring(0, amount.length - 2));
    const payerIdentification = urlParams.query.payerIdentification;
    const payerEmail = urlParams.query.payerEmail;
    const transactionId = urlParams.query.transactionId;
    const clientId = 1.23;

    const input = {
      sellerName,
      paymentId,
      orderId,
      amount: truncateAmount,
      payerIdentification,
      payerEmail,
      transactionId,
      clientId,
    };

    console.log('ohh', input);

    setIsTransactioning(true);

    createTransaction({
      variables: input,
    });
  }, [url]);

  useEffect(() => {
    console.log('-----url', url)
    if (url) {
      console.log('senddddd')
      sendTransaction();
    }
  }, [url, sendTransaction]);

  useEffect(() => {
    function _handleInitialUrl(urlIncoming) {
      console.log('Initial URL');
      if (urlIncoming) {
        console.log('URL is: ' + url);
        setUrl(urlIncoming);
      }
      else {
        navigation.navigate('HomeScreen');
      }
    }

    function _handleOpenUrl(event) {
      console.log('Open URL');
      if (event && event.url) {
        console.log('URL is: ' + event.url);
        setUrl(event.url);
      }
    }

    console.log('starting app');
    Linking.getInitialURL()
      .then(_handleInitialUrl)
      .catch(err => console.error('An error occurred', err));
    console.log('addEventListener url');
    Linking.addEventListener('url', _handleOpenUrl);

    return () => {
      console.log('removeEventListener url');
      Linking.removeEventListener('url', _handleOpenUrl);
    };
  }, []);

  return (
    <View style={styles.content}>
      { !dialog && 
        <View style={styles.container}>
          <Spinner style={{}} color="#F71963" size={100} type="Circle" />
          <Text style={styles.welcome}>
            Por favor inserta la tarjeta en el Datáfono.
          </Text>
          {/* <TouchableOpacity 
            style={globalStyle.buttons} 
            onPress = {onCancel}>
            <Text style={globalStyle.centerWhite}>Cancelar</Text>
          </TouchableOpacity> */}
        </View>
      }

      { dialog && 
        <ApprovedScreen onInvalid={onInvalid} buildUrlReturn={buildUrlReturn} />
      }
    </View>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -50,
  },
  container: {
    flex: 1,
    marginTop: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    flex: 1,
  },
  scrollViewContentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  welcome: {
    fontFamily: 'Fabriga',
    fontSize: 20,
    textAlign: 'center',
    margin: 50,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginTop: 5,
    marginBottom: 10,
  },
  test: {
    backgroundColor: '#333333',
  },

  contentDialog: {
    width: 500,
  }
});

export default PaymentProcessingHook;
