package com.redebanapplink;

import androidx.annotation.Nullable;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import android.content.ComponentName;
import android.content.ActivityNotFoundException;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableNativeArray;
import android.util.Log;

//Printer
import android.app.ProgressDialog;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.print.PrintManager;


/**
 * Expose Java to JavaScript. Methods annotated with {@link ReactMethod} are exposed.
 */
class ActivityStarterModule extends ReactContextBaseJavaModule {

   public static String PACKAGE = "rbm.pax.wimobile.com.rbmappcomercioswm";
   public static String SEND_COMERCIOS = "rbm.pax.wimobile.com.rbmappcomercios.features.mainmenu.ui.MainMenuActivity";
   public static String packageName = "package";
   public static String data_input = "data_input";
   Intent i;
   private static Integer REQUESTCOMERCIOS= 100;
   public final static int REQUEST_CODE_C = 3;

   ActivityStarterModule(ReactApplicationContext reactContext) {
       super(reactContext);
   }

   @Override
   public String getName() {
       return "ActivityStarter";
   }

   @ReactMethod
   public void navigateComercios(String jsonTransactionStr) {
      Log.i("Click2: ", jsonTransactionStr);
      String json = "{'TipoTransaccion':1,'properties': {'Monto': '400', 'Iva': '3',  'Inc': '3','Monto_base_iva': '3',   'Monto_base_inc': '3','Base_devolucion': '3'}}";
      Log.i("json", json);
      Activity currentActivity = getCurrentActivity();
      
      ComponentName cn = new ComponentName(PACKAGE, SEND_COMERCIOS);
      i = new Intent(Intent.ACTION_SEND);
      i.setComponent(cn);
      i.putExtra(data_input, jsonTransactionStr);
      i.putExtra(packageName, currentActivity.getPackageName());
      try {
         currentActivity.startActivityForResult(i, REQUESTCOMERCIOS);
      } catch (ActivityNotFoundException e) { 
         Toast.makeText(currentActivity.getApplicationContext(),"Toast por defecto", Toast.LENGTH_SHORT);
      }
   }



   // public void imprimir(
   //    String param1, String param2, String param3, String param4, String param5, 
   //    String param6, String param7){
   @ReactMethod
   public void imprimir(
         String param1, String param2, String param3, String param4, String param5, 
         String param6, String param7, String param8, String param9, String param10, 
         String param11, String param12, String param13, String param14, String param15){

      Activity currentActivity = getCurrentActivity();
      ComponentName cn = new ComponentName(currentActivity, ImpresionVTEX.class);
      i = new Intent(Intent.ACTION_SEND);
      i.setComponent(cn);
      i.putExtra("ParamTitulo", param1);
      i.putExtra("ParamNit", param2);
      i.putExtra("ParamDir", param3);
      i.putExtra("ParamTienda", param4);
      i.putExtra("ParamCaja", param5); 
      i.putExtra("ParamNoRecibo", param6);
      i.putExtra("ParamMaquina", param7);
      i.putExtra("ParamFecha", param8);
      i.putExtra("ParamHora", param9);
      i.putExtra("ParamOrderName", param10);
      i.putExtra("ParamOrderDoc", param11);
      i.putExtra("ParamOrderTotal", param12);
      i.putExtra("ParamForma", param13);
      i.putExtra("ParamInfoUrl", param14);
      i.putExtra("ParamVendedor", param15);
      
      
      currentActivity.startActivityForResult(i, 3);
  }

}
