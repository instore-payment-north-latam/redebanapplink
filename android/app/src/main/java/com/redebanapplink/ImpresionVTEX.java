package com.redebanapplink;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pax.dal.IDAL;
import com.pax.dal.IPrinter;
import com.pax.dal.entity.EFontTypeAscii;
import com.pax.dal.entity.EFontTypeExtCode;
import com.pax.dal.exceptions.PrinterDevException;
import com.pax.neptunelite.api.NeptuneLiteUser;
import java.util.Calendar;

public class ImpresionVTEX extends AppCompatActivity implements View.OnClickListener{
    private  int status;
    private ProgressDialog loader;
    private int greyValue = 100;
    public static final int TAG_Resultado = 3;

    public static final int FONT_BIG = 24;
    public static final int FONT_NORMAL = 20;

    public static final String TAG_PARAM_IMPRESION_P1 = "ParamTitulo";
    public static final String TAG_PARAM_IMPRESION_P2 = "ParamNit";
    public static final String TAG_PARAM_IMPRESION_P3 = "ParamDir";
    public static final String TAG_PARAM_IMPRESION_P4 = "ParamTienda";
    public static final String TAG_PARAM_IMPRESION_P5 = "ParamCaja";
    public static final String TAG_PARAM_IMPRESION_P6 = "ParamNoRecibo";
    public static final String TAG_PARAM_IMPRESION_P7 = "ParamMaquina";
    public static final String TAG_PARAM_IMPRESION_P8 = "ParamFecha";
    public static final String TAG_PARAM_IMPRESION_P9 = "ParamHora";
    public static final String TAG_PARAM_IMPRESION_P10 = "ParamOrderName";
    public static final String TAG_PARAM_IMPRESION_P11 = "ParamOrderDoc";
    public static final String TAG_PARAM_IMPRESION_P12 = "ParamOrderTotal";
    public static final String TAG_PARAM_IMPRESION_P13 = "ParamForma";
    public static final String TAG_PARAM_IMPRESION_P14 = "ParamInfoUrl";
    public static final String TAG_PARAM_IMPRESION_P15 = "ParamVendedor";
    
    private String P1 = null;
    private String P2 = null;
    private String P3 = null;
    private String P4 = null;
    private String P5 = null;
    private String P6 = null;
    private String P7 = null;
    private String P8 = null;
    private String P9 = null;
    private String P10 = null;
    private String P11 = null;
    private String P12 = null;
    private String P13 = null;
    private String P14 = null;
    private String P15 = null;

    private Button action;
    private TextView txImpresion;
    public static IDAL dal = null;
    String text_bloodgroup = null;

    private IPrinter printer;

    private NeptuneLiteUser neptuneUser = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        neptuneUser = NeptuneLiteUser.getInstance();

        if (neptuneUser != null) {
            Log.v("Impresion", "NO nulo");
            try {
                dal = neptuneUser.getDal(this);
                //dals = dalProxyClient.getDal(this.getApplicationContext());
                if(dal == null){
                    Log.v("Impresion", "dals null");
                }else {
                   printer = dal.getPrinter();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.v("Impresion", "dals null");
            }
        } else {
            Log.v("Impresion", "neptuneUser null");
        }

        Intent intent = getIntent();
        desEmpaquetarData(intent);
    }

    private void desEmpaquetarData( Intent intent) {
        Bundle extras = intent.getExtras();
        P1 = extras.getString(TAG_PARAM_IMPRESION_P1);
        P2 = extras.getString(TAG_PARAM_IMPRESION_P2);
        P3 = extras.getString(TAG_PARAM_IMPRESION_P3);
        P4 = extras.getString(TAG_PARAM_IMPRESION_P4);
        P5 = extras.getString(TAG_PARAM_IMPRESION_P5);
        P6 = extras.getString(TAG_PARAM_IMPRESION_P6);
        P7 = extras.getString(TAG_PARAM_IMPRESION_P7);
        P8 = extras.getString(TAG_PARAM_IMPRESION_P8);
        P9 = extras.getString(TAG_PARAM_IMPRESION_P9);
        P10 = extras.getString(TAG_PARAM_IMPRESION_P10);
        P11 = extras.getString(TAG_PARAM_IMPRESION_P11);
        P12 = extras.getString(TAG_PARAM_IMPRESION_P12);
        P13 = extras.getString(TAG_PARAM_IMPRESION_P13);
        P14 = extras.getString(TAG_PARAM_IMPRESION_P14);
        P15 = extras.getString(TAG_PARAM_IMPRESION_P15);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loader = new ProgressDialog(this);
        loader.setCancelable(false);
        loader.setTitle("Impresion");
        loader.setMessage("Imprimiendo...");
        loader.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        imprimir();

    }

    private void imprimir() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    printer.init();
                  
                    printer.printBitmap(BitmapFactory.decodeResource(ImpresionVTEX.this.getResources(), R.drawable.sony));
                    
                    printer.doubleHeight(false,false);
                    printer.doubleWidth(false,false);
                    printer.spaceSet(Byte.parseByte("1"),Byte.parseByte("1"));
                    //printer.fontSet(EFontTypeAscii.FONT_8_16, EFontTypeExtCode.FONT_24_48);
                    printer.fontSet(EFontTypeAscii.FONT_8_16, EFontTypeExtCode.FONT_24_48);
                    // printer.fontSet(EFontTypeAscii.FONT_8_16, EFontTypeExtCode.FONT_16_16);

                    printer.setGray(greyValue);
                    //printer.setFontPath(Environment.getExternalStorageDirectory() + "/RedebanComercios/aritmo.ttf");
                    
                    printer.setGray(500);
                    printer.printStr("\n",null);
                    printer.printStr(Centrar(P1,42),null);
                    printer.printStr(Centrar("NIT. "+P2,42),null);
                    printer.printStr(Centrar(P3,42),null);
                    printer.printStr(Centrar("Resolución DIAN:"+P4,42),null);
                    printer.printStr(Centrar("Prefijo POS del: 1 al: 1000",42),null);
                    printer.printStr(Centrar("Responsable de Iva",42),null);
                    printer.printStr("==========================================\n",null);
                    printer.printStr("\n",null);
                    printer.printStr(Centrar("FACTURA DE VENTA : POS - 14",42),null);
                    printer.printStr("Fecha         : "+P8+" Hora:"+P9+"\n",null);
                    printer.printStr("Cliente       : "+P10+"\n",null);
                    printer.printStr("Email         : "+P7+"\n",null);
                    printer.printStr("C.C / NIT     : "+P11+"\n",null);
                    printer.printStr("Dirección     : Calle 123\n",null);
                    printer.printStr("==========================================\n",null);
                    printer.printStr("\n",null);
                    printer.printStr(Alinear("(ct) Descripción","Valor",42),null);
                    printer.printStr(P6,null);
                    printer.printStr(Centrar("                    ______________________",42),null);
                    printer.printStr(Alinear("TOTAL:", P12, 42),null);
                    printer.printStr("==========================================\n",null);
                    printer.printStr("\n",null);
                    printer.printStr(Centrar("FORMA DE PAGO",42),null);
                    printer.printStr(Centrar(P13+" "+P12,42),null);
                    printer.printStr(Centrar("Cambio: 0",42),null);
                    printer.printStr(Centrar("Vendedor: "+P15,42),null);
                    // printer.printStr(Centrar(P5,42),null);
                    // printer.printStr(Centrar("No. :"+P6,42),null);
                    // printer.printStr(Centrar("Dispositivo: " + P7,42),null);        
                    // printer.printStr("__________________________________________\n",null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);
                    printer.printStr(Centrar("Elaborado por: SONY / POS",42),null);
                    printer.printStr(Centrar(P14,42),null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);
                    printer.printStr("\n",null);

                    status = printer.start();

                } catch (PrinterDevException e) {
                    e.printStackTrace();
                }

                loader.dismiss();
                setResult(status);
                finish();
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        setResult(4);
        finish();
    }

    

    private String Centrar(String s1, int maxCaracter) {
        String cadena = "";
        String espacios = "";
        int sizeS1, sizeS2, res, space;
        if(s1.length() >maxCaracter){
        }
        sizeS1 = s1.length();
        space = maxCaracter - (sizeS1);
        if (space >= 0) {
            for (int i = 0; i < space/2; i++) {
                espacios = espacios + " ";
            }
            cadena = espacios + s1 + espacios;
        } else {
            cadena = "error";
        }
        return cadena+"\n";
    }


    private String Alinear(String s1, String s2, int maxCaracter) {
        String cadena = "";
        String espacios = "";
        int sizeS1, sizeS2, res, space;
        sizeS1 = s1.length();
        sizeS2 = s2.length();
        space = maxCaracter - (sizeS1 + sizeS2);
        if (space > 0) {
            for (int i = 0; i < space; i++) {
                espacios = espacios + " ";
            }
            cadena = s1 + espacios + s2 ;
        } else {
            cadena = "error" ;
        }
        return cadena+ "\n";
    }

    private String[] centrarParrafo(String parrafo,int numCaracter){
        String[] result;
        String[] resultTemp = new String[10];
        String[] temp;
        String tempD="";
        String tempD2="";
        int lineas = 0;
        int numLineas = 0;
        if(parrafo == null){
            return  null;
        }
        temp = parrafo.split(" ");

        for(int i=0;i<temp.length;i++){
            tempD = tempD+" "+temp[i];
            if(tempD.length() > numCaracter){
                i--;
                resultTemp[numLineas] = Centrar(tempD2,numCaracter);
                numLineas++;
                tempD = "";
                tempD2 = "";
            }else {
                tempD2 = tempD;
            }
        }
        if(!tempD.equals("")){
            numLineas++;
            resultTemp[numLineas-1] = Centrar(tempD,numCaracter);
        }
        result = new String[numLineas];
        for(int i=0;i<numLineas;i++){
            result[i]=resultTemp[i];
        }
        return result;
    }

    public String statusCode2Str(int status){
        String res="";
        switch (status) {
            case 0:
                res = "Impresion exitosa";
                break;
            case 1:
                res = "Impresora ocupada";
                break;
            case 2:
                res = "Sin papel";
                break;
            case 3:
                res = "El formato de impresión de error de paquetes de datos";
                break;
            case 4:
                res = "mal funcionamiento de la impresora";
                break;
            case 8:
                res = "Impresora se sobrecalienta ";
                break;
            case 9:
                res = "voltaje de la impresora es demasiado baja";
                break;
            case 240:
                res = "La impresión esta sin terminar ";
                break;
            case 252:
                res = "La impresora no se ha instalado la librería de fuentes ";
                break;
            case 254:
                res = "paquete de datos es demasiado largo ";
                break;
            default:
                break;
        }
        return res;
    }
}
