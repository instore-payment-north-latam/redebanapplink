import * as UrlNode from 'url';
import moment from 'moment';

export const buildResponseUrl = (
  url,
  cardNumber,
  cardBrandName,
  approveCode,
  date,
  receiptCode,
  accountType,
  installmentsPayment,
) => {
  if (!url || url === null || url === '') {
    return 'instore://payment-reversal?responsecode=110&reason=app+refused&paymentId=000';
  }

  date = date ? date : moment().format();
  receiptCode = receiptCode ? receiptCode : '';
  accountType = accountType ? accountType : '';
  installmentsPayment = installmentsPayment
    ? 'Numero de Cuotas=' + installmentsPayment
    : '';

  const urlParams = UrlNode.parse(url, true);
  const paymentId = urlParams.query.paymentId;
  const sellerName = urlParams.query.sellerName;
  const value = urlParams.query.value;

  const receipt =
    sellerName +
    '\n' +
    '**RECIBO**\n' +
    'REDEBAN\n' +
    cardBrandName +
    ' - ' +
    accountType +
    '\n' +
    '**** **** ****' +
    cardNumber +
    '\n' +
    date +
    '\n' +
    'VALOR= ' +
    value +
    '\n' +
    installmentsPayment +
    '\n' +
    'RECIBO= ' +
    receiptCode;

  let urlResponse = 'instore://payment?responsecode=0&';
  urlResponse += [
    'acquirer=0',
    'acquirerName=redeban',
    `cardBrandName=${cardBrandName}`,
    `merchantReceipt=${receipt}`,
    `customerReceipt=`,
    `paymentId=${paymentId}`,
    `firstDigits=${cardNumber.split('**')[0]}`,
    `lastDigits=${cardNumber.split('**')[1]}`,
    `acquirerAuthorizationCode=${approveCode}`,
  ].join('&');
  // console.log('urlResponse: ', urlResponse);
  return urlResponse;
};

export const buildFailedResponseUrl = (paymentId, errorMessage) => {
  let urlResponse = `instore://payment?responsecode=110&paymentId=${paymentId}&reason=${errorMessage}`;
  return urlResponse;
};
