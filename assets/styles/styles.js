import { StyleSheet, Dimensions } from 'react-native'

var width = Dimensions.get('window').width;
var primaryColor = "#F71963";
var secondColor = "#142032";

const globalStyle = StyleSheet.create({
  container: {
   margin: 20,
   display: 'flex',
   flex: 1,
   justifyContent: 'center',
  },

  paragraph: {
    fontFamily: 'Fabriga',
    fontSize: 20,
  },

  scanQr: {
    backgroundColor: secondColor,
    color: '#FFFFFF',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
    borderRadius: 10
  },

  customView: {
  },

  btnTop: {
    backgroundColor: '#ff3264',
    color: 'white',
  },
  
  logoVtex: {
    width: 250,
    resizeMode: 'contain',
  },

  figure: {
    flex: 1,
    display: 'flex',
    alignItems: 'center'
  },

  headerSides: {
    marginLeft: 10,
    marginRight: 10,
    padding: 25,
  },

  headerSettings: {
    marginLeft: 10,
  },

  centerWhite: {
    color: '#FFF',
    textAlign: 'center',
  },

  globalBtn: {
    backgroundColor: '#ff3264',
    padding: 5,
    width: 200,
    borderRadius: 5,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },

  centerRow: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },

  textCenter: {
    textAlign: 'center'
  },

  buttons: {
    margin: 5,
    backgroundColor: '#F71963',
    padding: 15,
    borderRadius: 4,
    width: 140,
  },

})


const styles = StyleSheet.create({
  container: {
    margin: 20,
  },

  custom: {
    padding: 50,
    flex: 1
  },

  header: {
    backgroundColor: 'black',
  },

  headerText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },

  btnCustom: {
    margin: 20,
  },

  inputCustom: {
    paddingLeft: 20,
    borderColor: '#CCC',
    borderWidth: 1,
    height: 40,
    borderRadius: 4,
    marginBottom: 20,
  },

  customPicker: {
    height: 38,
    marginLeft: -20,
  },

  titles: {
    fontSize: 18,
    textAlign: 'left',
  },

  sectionCollapsed: {
    marginBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
  },

  infoCollapsed: {
    paddingTop: 15,
    padding: 10,
  },

  contentCollapsed: {
    padding: 10,
  },

  separeCollapsed: {
    borderBottomWidth: 1,
    borderBottomColor: '#00000008',
  },

  btnSave: {
    backgroundColor: '#F71963',
    padding: 15,
    borderRadius: 4,
  },

  checkbox: {
    padding: 25,
    paddingRight: 40,
    paddingLeft: 0,
  },

  textSave: {
    color: '#FFF',
    textAlign: 'center',
    fontWeight: 'bold',
  },

  hide: {
    backgroundColor: 'red',
  },

  requiredSetting: {
    position: 'absolute',
    top: -20,
    padding: 10,
    backgroundColor: '#ff3264',
    width: width,
  },

  msmSpanRequired: {
    color: '#F71963',
  },

  msmRequired: {
    marginTop: -10,
    fontStyle: 'italic',
    marginBottom: 10,
    fontSize: 12,
  },

  textAlert: {
    color: '#FFF',
    fontSize: 16,
    textAlign: 'center',
  },

  btnLogin: {
    backgroundColor: '#ff3264',
    marginTop: 50,
    padding: 10,
    borderRadius: 4,
    width: 200,
  },

  containerLogin: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },

  titleSettings: {
    fontSize: 20,
    color: primaryColor,
    fontWeight: "bold",
    marginVertical: 10,
  },

  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center'
  },

  bottomOverlay: {
    bottom: 0,
    backgroundColor: '#F71963',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },

  scanText: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFF',
    textAlign: 'center',
  },

  cameraView: {
     flex: 1,
     justifyContent: 'flex-start',
  }
  

})

const approvedStyle = StyleSheet.create({
  contentDialog: {
    width: 400,
    padding: 0,
  },
  headerDialog: {
    marginLeft: -18,
    marginRight: -18,
    display: 'flex',
  },
  title: {
    textAlign: 'center',
    color: secondColor,
    fontSize: 30,
    fontWeight: 'bold',
  },
  icon: {
    fontSize: 40,
    display: 'flex',
    alignItems: 'center',
    margin: 30,
  },

  contentButtons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
  },

  contentButtonsScan: {
    flexDirection: 'column',
  },

  alignButtons: {
    alignItems: 'center',
    margin: 24,
  },

  buttons: {
    margin: 10,
    width: 150,
    backgroundColor: primaryColor,
    padding: 15,
    borderRadius: 5,
  },

  cancelButton: {
    backgroundColor: secondColor,
  },

  btnScann: {
    backgroundColor: "#134cd8",
    marginTop: 40,
    width: 150,
    padding: 15,
    borderRadius: 5,
  },

  btnScannText: {
    color: "#FFFFFF",
    fontWeight: "bold",
    textAlign: "center"
  }

})

const buttons = StyleSheet.create({
  primary: {
    marginRight: 100
  }
})

const newPaymentStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
})
 
export { styles, buttons, globalStyle, approvedStyle, newPaymentStyle } 